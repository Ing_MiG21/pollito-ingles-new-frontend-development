'use strict';

describe('Service: responseErrorInterceptor', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var responseErrorInterceptor;
  beforeEach(inject(function (_responseErrorInterceptor_) {
    responseErrorInterceptor = _responseErrorInterceptor_;
  }));

  it('should do something', function () {
    expect(!!responseErrorInterceptor).toBe(true);
  });

});
