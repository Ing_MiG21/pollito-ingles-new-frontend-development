'use strict';

describe('Service: SearchResource', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var SearchResource;
  beforeEach(inject(function (_SearchResource_) {
    SearchResource = _SearchResource_;
  }));

  it('should do something', function () {
    expect(!!SearchResource).toBe(true);
  });

});
