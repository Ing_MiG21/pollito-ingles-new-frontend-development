'use strict';

describe('Service: advancedsearchresource', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var advancedsearchresource;
  beforeEach(inject(function (_advancedsearchresource_) {
    advancedsearchresource = _advancedsearchresource_;
  }));

  it('should do something', function () {
    expect(!!advancedsearchresource).toBe(true);
  });

});
