'use strict';

describe('Service: NotificationsResource', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var NotificationsResource;
  beforeEach(inject(function (_NotificationsResource_) {
    NotificationsResource = _NotificationsResource_;
  }));

  it('should do something', function () {
    expect(!!NotificationsResource).toBe(true);
  });

});
