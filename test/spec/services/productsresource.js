'use strict';

describe('Service: ProductsResource', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var ProductsResource;
  beforeEach(inject(function (_ProductsResource_) {
    ProductsResource = _ProductsResource_;
  }));

  it('should do something', function () {
    expect(!!ProductsResource).toBe(true);
  });

});
