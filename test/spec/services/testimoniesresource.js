'use strict';

describe('Service: TestimoniesResource', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var TestimoniesResource;
  beforeEach(inject(function (_TestimoniesResource_) {
    TestimoniesResource = _TestimoniesResource_;
  }));

  beforeEach(inject(function($injector) {
     // Set up the mock http service responses
     $httpBackend = $injector.get('$httpBackend');
     // backend definition common for all tests
     authRequestHandler = $httpBackend.whenPOST('/api/parents/testimonies').respond({userId: 'userX'}, {'A-Token': 'xxx'});
   }));


  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
   });

  it('should post a new testimonie', function () {
      //Create an expectation for the correct url, and respond with a mock object
      $httpBackend.whenPOST('/parents/testimonies', {'Token':'xxx'}).respond(200, {"detail": "Your testimonie has been added"});

      //Make the query
      resource.$query();

      //Because we're mocking an async action, ngMock provides a method for us to explicitly flush the request
      $httpBackend.flush();

      //Now the resource should behave as expected
      expect(resource.name).toBe('test');
      expect(!!TestimoniesResource).toBe(true);
  });

});
