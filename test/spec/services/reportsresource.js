'use strict';

describe('Service: ReportsResource', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var ReportsResource;
  beforeEach(inject(function (_ReportsResource_) {
    ReportsResource = _ReportsResource_;
  }));

  it('should do something', function () {
    expect(!!ReportsResource).toBe(true);
  });

});
