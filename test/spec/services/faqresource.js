'use strict';

describe('Service: FaqResource', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var FaqResource;
  beforeEach(inject(function (_FaqResource_) {
    FaqResource = _FaqResource_;
  }));

  it('should do something', function () {
    expect(!!FaqResource).toBe(true);
  });

});
