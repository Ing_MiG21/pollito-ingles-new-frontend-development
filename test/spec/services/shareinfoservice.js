'use strict';

describe('Service: ShareInfoService', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var ShareInfoService;
  beforeEach(inject(function (_ShareInfoService_) {
    ShareInfoService = _ShareInfoService_;
  }));

  it('should do something', function () {
    expect(!!ShareInfoService).toBe(true);
  });

});
