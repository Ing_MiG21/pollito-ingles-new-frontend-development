'use strict';

describe('Service: ParentsResource', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var ParentsResource;
  beforeEach(inject(function (_ParentsResource_) {
    ParentsResource = _ParentsResource_;
  }));

  it('should do something', function () {
    expect(!!ParentsResource).toBe(true);
  });

});
