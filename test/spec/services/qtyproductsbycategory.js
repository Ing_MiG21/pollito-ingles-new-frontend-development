'use strict';

describe('Service: QtyProductsByCategory', function () {

  // load the service's module
  beforeEach(module('pollitoinglesApp'));

  // instantiate service
  var QtyProductsByCategory;
  beforeEach(inject(function (_QtyProductsByCategory_) {
    QtyProductsByCategory = _QtyProductsByCategory_;
  }));

  it('should do something', function () {
    expect(!!QtyProductsByCategory).toBe(true);
  });

});
