'use strict';

describe('Controller: SearchcategoryCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var SearchcategoryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SearchcategoryCtrl = $controller('SearchcategoryCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SearchcategoryCtrl.awesomeThings.length).toBe(3);
  });
});
