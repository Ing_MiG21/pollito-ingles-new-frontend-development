'use strict';

describe('Controller: ActivationCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var ActivationCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ActivationCtrl = $controller('ActivationCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ActivationCtrl.awesomeThings.length).toBe(3);
  });
});
