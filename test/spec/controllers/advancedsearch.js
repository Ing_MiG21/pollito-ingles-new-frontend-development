'use strict';

describe('Controller: AdvancedsearchCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var AdvancedsearchCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdvancedsearchCtrl = $controller('AdvancedsearchCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AdvancedsearchCtrl.awesomeThings.length).toBe(3);
  });
});
