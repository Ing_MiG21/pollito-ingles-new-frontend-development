'use strict';

describe('Controller: UsersprofiletabsCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var UsersprofiletabsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UsersprofiletabsCtrl = $controller('UsersprofiletabsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(UsersprofiletabsCtrl.awesomeThings.length).toBe(3);
  });
});
