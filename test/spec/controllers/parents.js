'use strict';

describe('Controller: ParentsctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var ParentsctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentsctrlCtrl = $controller('ParentsctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ParentsctrlCtrl.awesomeThings.length).toBe(3);
  });
});
