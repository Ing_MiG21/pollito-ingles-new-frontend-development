'use strict';

describe('Controller: Faq2Ctrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var Faq2Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Faq2Ctrl = $controller('Faq2Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(Faq2Ctrl.awesomeThings.length).toBe(3);
  });
});
