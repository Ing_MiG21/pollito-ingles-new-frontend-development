'use strict';

describe('Controller: TpRecommendedCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var TpRecommendedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TpRecommendedCtrl = $controller('TpRecommendedCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TpRecommendedCtrl.awesomeThings.length).toBe(3);
  });
});
