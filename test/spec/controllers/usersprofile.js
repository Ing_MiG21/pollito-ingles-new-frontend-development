'use strict';

describe('Controller: UsersprofileCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var UsersprofileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UsersprofileCtrl = $controller('UsersprofileCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(UsersprofileCtrl.awesomeThings.length).toBe(3);
  });
});
