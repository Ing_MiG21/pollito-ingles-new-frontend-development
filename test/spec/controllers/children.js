'use strict';

describe('Controller: ChildrenCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var ChildrenCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ChildrenCtrl = $controller('ChildrenCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ChildrenCtrl.awesomeThings.length).toBe(3);
  });
});
