'use strict';

describe('Controller: FollowersCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var FollowersCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FollowersCtrl = $controller('FollowersCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FollowersCtrl.awesomeThings.length).toBe(3);
  });
});
