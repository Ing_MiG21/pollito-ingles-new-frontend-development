'use strict';

describe('Controller: AdvancedsearchresultsctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var AdvancedsearchresultsctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdvancedsearchresultsctrlCtrl = $controller('AdvancedsearchresultsctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AdvancedsearchresultsctrlCtrl.awesomeThings.length).toBe(3);
  });
});
