'use strict';

describe('Controller: UserproductprofileCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var UserproductprofileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserproductprofileCtrl = $controller('UserproductprofileCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(UserproductprofileCtrl.awesomeThings.length).toBe(3);
  });
});
