'use strict';

describe('Controller: ChildrenmodalCtrl', function () {

  // load the controller's module
  beforeEach(module('pollitoinglesApp'));

  var ChildrenmodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ChildrenmodalCtrl = $controller('ChildrenmodalCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ChildrenmodalCtrl.awesomeThings.length).toBe(3);
  });
});
