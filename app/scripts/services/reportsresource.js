'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.ReportsResource
 * @description
 * # ReportsResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('ReportsResource', ['$resource', 'API', function ($resource, API) {

      return $resource(API.reports, {id_report: '@id'}, {
          getReportTypes: {
              method: 'GET',
              url: API.reports,
              isArray: false
          },
          reportUser: {
              method: 'POST',
              url: API.reports + 'users/'
          },
          getUsersReports: {
              method: 'GET',
              url: API.reports + 'users/'
          }
      });

}]);
