'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.QtyProductsByCategory
 * @description
 * # QtyProductsByCategory
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('QtyProductsByCategoryResource', ['API', '$resource', function (API, $resource) {
    return $resource(API.categories, {}, {        
        getQtyProductsByCategory: {
              method: 'GET',
              url: API.categories + "qty_prod_by_cat/",              
          }
    });
  }]);
