'use strict';

angular
	.module('pollitoinglesApp')
	.factory('ZoomResource', ["$resource", "zoomUrl", function ($resource, zoomUrl) {

		return $resource(zoomUrl, {}, {
      
      getEstados: {
        method: 'JSONP',
        url: zoomUrl + 'getEstados',
        params:  {callback: "JSON_CALLBACK"},
        isArray: true
      },
      getCiudades: {
        method: 'POST',
        url: zoomUrl + 'getCiudadesOfi',
        isArray: true
      },
      getSucursales: {
        method: 'POST',
        url: zoomUrl + 'getSucursales',
        isArray: true
      }

    });
  }]);
