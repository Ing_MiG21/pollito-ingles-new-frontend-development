'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.ProductsResource
 * @description
 * # ProductsResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
.factory('ProductsResource', ['API', '$resource', function (API, $resource) {
    return $resource(API.products, {id: '@id'}, {
        getProducts: {
            method: 'GET',
            url: API.products,
            isArray: true
        },
        update: {
            method: 'PUT',
            url: API.products + ':id/',
            params: {
                id: '@id'
            }
        },
        getLabels: {
            method: 'GET',
            url: API.products + 'labels/list/'
        },
        addLabels: {
            method: 'POST',
            url: API.products + ':id_product/labels/:id_label/',
            params: {
                id_product: '@product_id',
                id_label: '@label_id'
            }
        },
        getCategories: {
            method: 'GET',
            url: API.categories + 'list/'
        },
        addImages: {
            method: 'POST',
            url: API.products + 'images/'
        },
        getImages: {
            method: 'GET',
            url: API.products + 'images/'
        },
        updateImages: {
            method: 'PUT',
            url: API.products + 'images/:id/',
            params: {
                id: '@id'
            }
        },
        addPublication: {
            method: 'POST',
            url: API.publications
        },
        getPublication: {
            method: 'GET',
            url: API.publications + ':id/',
            params: {
                id: '@id'
            }
        },
        getPublications: {
            method: 'GET',
            url: API.publications
        },
        getPublicationsFrom: {
            method: 'GET',
            url: API.publications + '?uid=:uid',
            params: {
                uid: '@uid'
            }
        },
        updatePublication: {
            method: 'PUT',
            url: API.publications + ':id/',
            params: {
                id: '@id'
            }
        },
        deletePublication: {
            method: 'DELETE',
            url: API.publications + ':id/',
            params: {
                id: '@id'
            }
        },
        getMorePublications: {
            method: 'GET',
            url: API.publications + 'user_random/?uid=:uid',
            params: {
                uid: '@uid'
            }
        },
        getCountries: {
            method: 'GET',
            url: API.locations + 'countries/'
        },
        getRegions: {
            method: 'GET',
            url: API.locations + 'regions/'
        },
        getCities: {
            method: 'GET',
            url: API.locations + 'cities/?q=:region',
            params: {
                region: '@region'
            }
        },
        addInapropiate: {
            method: 'POST',
            url: API.products + 'inapropiate/'
        },
        getPublicationsTiendaPollito: {
            method: 'GET',
            url: API.publications + '?tp=:1'
        },
        getFeaturedTiendaPollito: {
            method: 'GET',
            url: API.publications + 'featured/'
        },
        getBestSellerTiendaPollito: {
            method: 'GET',
            url: API.publications + 'best_seller/'
        },
        getProduct: {
            method: 'GET',
            url: API.products + ':id/',
            params: {
                id: '@id'
            }
        },
    });
}]);
