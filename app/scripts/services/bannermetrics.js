'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.BannersMetricsResource
 * @description
 * # BannersMetricsResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('BannersMetricsResource', ['$resource', 'API', function ($resource, API) {

      return $resource(API.metricasban, {}, {
          get: {
            url: API.metricasban,
            method: 'GET'  
          },
          post: {
          	url: API.metricasban,
          	method: 'POST'
          },
          update:{
          	url: API.metricasban + ':id',
          	method: 'PUT',
          	params: {
              id: 'id'
          	}
          }
      });
}]);