'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.TestimoniesResource
 * @description
 * # TestimoniesResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('TestimoniesResource', ['$resource', 'API', function ($resource, API) {
      return $resource(API.parents, {}, {
      	get: {
      		url: API.parents + 'testimonies/',
      		method: 'GET',
      	},
      	getRandomTestimonies: {
      		url: API.parents + 'testimonies/random_testimonies/',
      		method: 'GET',
      		isArray: true
      	}
      }); 
  }
]);
