'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.Regex
 * @description
 * # Regex
 * Constant in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .constant('Regex', {
      'names': '[A-zÀ-ÿ]{2,25}(\\s[A-zÀ-ÿ]{2,25})?',
      'cellphone': '[+]?[\\d]{10,13}',
      'price': '\\d{1,11}(?:[.]\\d{2})?',
      'alphaonly': '/[a-zA-ZñÑ ]/',
      'numberonly': '/[0-9]/'
  });
