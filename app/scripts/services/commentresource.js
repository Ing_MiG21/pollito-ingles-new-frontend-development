'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.FaqResource
 * @description
 * # FaqResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('CommentResource', ['$resource', 'API', function ($resource, API) {

        return $resource(API.comments, {}, {
	          get: {
	              method: 'GET',
	              url: API.comments + '?instance=:id',
	              isArray: true,
                  params: {
          	    	id: '@id'
          		}
	          },
            post: {
                method: 'POST',
                url: API.comments
            }
        });
}]);
/*
      this.comments = this.toUrl('/api/publications/comments/');
    this.contentType = this.toUrl('/api/publications/comments-content-type/');*/