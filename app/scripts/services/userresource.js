'use strict';

angular
	.module('pollitoinglesApp')
	.factory('UserResource', ["$resource", "API", function ($resource, API) {
		return $resource(API.users, {}, {
      create: {
        url: API.users,
        method: 'POST'
      },
      getUser: {
        url: API.users + ':id_user/',
        method: 'GET',
		    params: {
          id_user: '@id_user'
		    }
      },
      getUserByEmail: {
        url: API.users + 'email/:email/',
        method: 'GET',
        params: {
          emial: '@email'
        }
      },
      getAllUsers: {
        url: API.about_us,
        method: 'GET',
        isArray: true
      },
      update: {
        url: API.users + ':id_user/',
		    method: 'PUT',
		    params: {
          id_user: '@id_user'
		    }
      },
      retrievePassword: {
        url: API.users + 'retrieve-password/email/',
        method: 'POST'
      },
      newPassword: {
        url: API.users + 'retrieve-password/:token/',
        params: {
          token: '@token'
        },
        method: 'POST'
      },
      activate: {
        url: API.users + "activate/:token/",
        method: 'GET'
      },
      reactivate: {
        url: API.users + "send-activation/email/",
        method: 'POST'
      },
      contactUs: {
        url: API.about_us + "contact_us/",
        method: 'POST'
      }
    });
}]);
