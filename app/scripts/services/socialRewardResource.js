'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.SocialSharedRewardResource
 * @description
 * # SocialSharedRewardResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('SocialRewardResource', ['$resource', 'API', function ($resource, API) {

      return $resource(API.socialShared, {}, {
          postSocial: {
            url: API.socialShared,
            method: 'POST'  
          }
      });

}]);

