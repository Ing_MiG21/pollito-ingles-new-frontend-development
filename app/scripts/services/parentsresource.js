'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.ParentsResource
 * @description
 * # ParentsResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('ParentsResource', ['$resource', 'API', function ($resource, API) {
    return $resource(API.parents, {}, {
      follow: {
        method: 'POST',
        url: API.parents + 'following/:id_user/',
        params: {
            id_user: '@id_user'
        }
      },
      unfollow: {
        method: 'DELETE',
        url: API.parents + 'following/:id_user/',
        params: {
            id_user: '@id_user'
        }
      },
      followers: {
        method: 'GET',
        url: API.parents + 'followers/?uid=:uid',
        params: {
            uid: '@uid'
        }
      },
      following: {
        method: 'GET',
        url: API.parents + 'i_follow/?uid=:uid',
        params: {
            uid: '@uid'
        }
      },
      profile: {
        method: 'GET',
        url: API.parents + 'profile/:id_user/',
        params: {
          id_user: '@id'
        }
      },
      update: {
        method: 'PUT',
        url: API.parents + 'update/'
      },
      reputation: {
        method: 'POST',
        url: API.parents + 'reputation/'
      },
      getReputation: {
        method: 'GET',
        url: API.parents + 'profile/total_reputation/:id_user/',
        isArray: true,
        params: {
          id_user: '@id_user'
        }
      },
      addSuggestion: {
          method: 'POST',
          url: API.parents + 'suggestions/'
      },
      addChild: {
          method: 'POST',
          url: API.children
      },
      getChildren: {
          method: 'GET',
          url: API.children
      },
      updateChild: {
          method: 'PUT',
          url: API.children + ':id/',
          params: {
              id: 'id'
          }
      },
      deleteChild: {
          method: 'DELETE',
          url: API.children + ':id/',
          params: {
              id: 'id'
          }
      },
      getRewards:{
        method: 'GET',
        url: API.parents + 'rewards/',
      },
      getMessages:{
        method: 'GET',
        url: API.parents + 'message_private/talk/:uid/',
        params: {
            uid: '@uid'
        }
      },
      getTalkers:{
        method: 'GET',
        url: API.parents + 'message_private/talkers/',
      },
      postMessages:{
        method: 'POST',
        url: API.parents + 'message_private/',
      },
      deleteConversation:{
        method: 'DELETE',
        url: API.parents + 'message_private/talkers/:sid/',
        params: {
            sid: '@sid'
        }
      },
      checkPlanProducts:{
        method: 'GET',
        url: API.parents + 'check_plan/',
      },
      income:{
        method: 'GET',
        url: API.parents + 'income/',
      },
      websites: {
        method: 'GET',
        url: API.parents + 'web-sites/?ht=:ht',
        params: {
            ht: '@ht'
        }
      },
      registryComplete: {
        method: 'POST',
        url: API.parents + 'complete_register/',
      },
      registryCompleteWebSites: {
        method: 'POST',
        url: API.parents + 'complete_register/web-sites/',
      },
      checkRegistry: {
        method: 'GET',
        url: API.parents + 'complete_register/check/',
      }
    });
  }]);
