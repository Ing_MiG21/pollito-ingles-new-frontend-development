'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.Regions
 * @description
 * # Regions
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('RegionsResource', ["$resource", "API", function ($resource, API) {
    return $resource(API.locations, {}, {
      getRegions: {
        url: API.locations + 'region/?cid=:cid',
        method: 'GET',
      }
    });
  }]);
