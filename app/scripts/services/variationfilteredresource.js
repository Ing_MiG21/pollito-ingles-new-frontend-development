'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.VariationFilteredResource
 * @description
 * # VariationFilteredResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
    .factory('VariationFilteredResource', ['$resource', 'API', function ($resource, API) {
        return $resource(API.variation_filtered, {}, {
	    	single: {
	        	method: 'GET',
	        	url: API.variation_filtered + ':id_product/:id_variation/',
	        	params: {
	                id_product: '@id_product', 
	                id_variation: '@id_variation'
	            }
	    	},
	    	double: {
	        	method: 'GET',
	        	url: API.variation_filtered + ':id_product/:id_variation/:id_variationTwo/',
	        	params: {
	                id_product: '@id_product', 
	                id_variation: '@id_variationOne', 
	                id_variationtwo: '@id_variationTwo'
	            }
	    	}
        });
}]);