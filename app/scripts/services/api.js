'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.API
 * @description
 * # API
 * Service in the pollitoinglesApp.
 */

angular
  .module('pollitoinglesApp')
  .service('ShareInfoService', function () {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var resultSet = null;

            return {
                getProperty: function () {
                    return resultSet;
                },
                setProperty: function(value) {
                    resultSet = value;
                }
            };

      })
  .service('API',['baseUrl', function (baseUrl, zoomUrl) {

    this.toUrl = function(url) {
      return baseUrl + url;
    };

    this.faq = this.toUrl('/api/faq/');
    this.users = this.toUrl('/api/users/');
    this.parents = this.toUrl('/api/parents/');
    this.reports = this.toUrl('/api/reports/');
    this.children = this.toUrl('/api/children/');
    this.products = this.toUrl('/api/products/');    
    this.login = this.toUrl('/api/users/login/');
    this.logout = this.toUrl('/api/users/logout/');
    this.locations = this.toUrl('/api/ubicacion/');
    this.publications = this.toUrl('/api/publications/');
    this.notifications = this.toUrl('/api/notifications/');
    this.categories = this.toUrl('/api/products/categories/');
    this.searchCategories = this.toUrl('/api/publications/category_search/');
    this.generalSearch = this.toUrl('/api/publications/general_search/');
    this.advancedSearch = this.toUrl('/api/publications/advanced_search/');
    this.comments = this.toUrl('/api/publications/comments/');
    this.contentType = this.toUrl('/api/publications/comments-content-type/');
    this.orders = this.toUrl('/api/orden-de-compra/');
    this.message_private = this.toUrl('/api/parents/message_private/');
    this.memberships = this.toUrl('/api/memberships/');
    this.about_us = this.toUrl('/api/about-us/');
    this.banners = this.toUrl('/api/publicity/');
    this.socialShared = this.toUrl('/api/parents/social_share_reward/');
    this.metricasban = this.toUrl('/api/publicity/metricas/');
    this.variation = this.toUrl('/api/products/variation/');
    this.variation_filtered = this.toUrl('/api/products/product_variation/');
  }]);
