'use strict';

angular
	.module('pollitoinglesApp')
	.factory('MembershipsResource', ["$resource", "API", function ($resource, API) {
		return $resource(API.memberships, {}, {
	  
			getDiscounts: {
				method: 'GET',
				url: API.memberships + 'incentives/',
			},
			getPlans: {
		        method: 'GET',
		        url: API.memberships + 'plans/',
		    },
		    getPlan: {
		        method: 'GET',
		        url: API.memberships + 'plans/:pid/',
		        params: {
		            pid: '@pid'
		        }
		    },
		    getPlanTotals: {
		        method: 'GET',
		        url: API.memberships + 'plans_totals/:pid/',
		        params: {
		            pid: '@pid'
		        }
		    },
		    subscription: {
		    	method: 'POST',
		        url: API.memberships + 'subscription/',
		    }
		});
	}]);
