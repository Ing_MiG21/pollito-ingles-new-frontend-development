'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.FaqResource
 * @description
 * # FaqResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('ContenttypeResource', ['$resource', 'API', function ($resource, API) {

        return $resource(API.contentType, {}, {
	          get: {
              method: 'GET',
              url: API.contentType
	          }
        });
}]);
/*
      this.comments = this.toUrl('/api/publications/comments/');
    this.contentType = this.toUrl('/api/publications/comments-content-type/');*/