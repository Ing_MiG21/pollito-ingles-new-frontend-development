'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.SearchResource
 * @description
 * # SearchResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('SearchResource', ['$resource', 'API', function ($resource, API) {
      return $resource(API.search, {}, {
          searchItem: {
              method: 'GET',              
              url: API.generalSearch + ':item/:is_chick/',
              params: {
                  item: '@item',
                  is_chick: '@is_chick'
              },
              isArray: true
          },  
          searchCategory: {
              method: 'GET',              
              url: API.searchCategories + ':item/:is_chick/',
              params: {
                  item: '@item',
                  is_chick: '@is_chick'
              },
              isArray: true
          },
      });
  }]);
