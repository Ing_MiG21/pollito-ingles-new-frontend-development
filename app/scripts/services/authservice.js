'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.AuthService
 * @description
 * # AuthService
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('AuthService', function ($http, SessionService, API, $rootScope) {

    var authService = {}

    authService.login = function(credentials){
      return $http.post(API.login, credentials)
        .then(function(res){
          SessionService.create(res.data);
          SessionService.setEmail(credentials.username);
          return res;
        });
    };

    authService.logout = function(){
      return $http.post(API.logout)
        .then(function(res){
            SessionService.destroy(res.data);
            SessionService.deleteEmail();
            SessionService.deleteId();
            SessionService.deleteName();
            SessionService.deleteLastname();
            SessionService.deletePhone();
            SessionService.deleteAddress();
            SessionService.deleteDataComplete();
            SessionService.deleteCart();
            $rootScope.UserName = null;
            $rootScope.incentives = false;
            // TODO delete log
            FB.getLoginStatus(function(response) {
                if (response.status == 'connected') {
                    FB.logout(function() {});
                }
            });
            return res;
        });
    };

    return authService;

  });
