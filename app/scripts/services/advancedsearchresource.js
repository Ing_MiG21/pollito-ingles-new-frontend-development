'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.advancedsearchresource
 * @description
 * # advancedsearchresource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('AdvancedSearchResource', ['API', '$resource', function (API, $resource) {
      return $resource(API.advancedSearch + ':look_at/:age/:gender/:reputation/:city/:price/',{ look_at: '@look_at', age: '@age', gender: '@gender', reputation: '@reputation', city: '@city', price:'@price' },{
          advanceSearch: {
              method: 'GET',
              url: API.advancedSearch + ':look_at/:age/:gender/:reputation/:city/:price/',
              params: {
                look_at:    '@look_at',
                age:        '@age',
                gender:     '@gender',
                reputation: '@reputation',
                city:       '@city',
                price:      '@price'
              },
              isArray: true
          },                
      });
  }]);
