'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.responseErrorInterceptor
 * @description
 * # responseErrorInterceptor
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('responseErrorInterceptor', ['toaster', '$q', 'SessionService', function (toaster, $q, SessionService) {
    return {
        responseError: function(rejection) {
            //TODO delete this log
            console.log(rejection);
            var show = true;

            switch (rejection.status) {
                case 404:
                    if('data' in rejection){
                        if('detail' in rejection.data){
                            toaster.pop('error', '', rejection.data.detail);
                        }
                        if('email' in rejection.data){
                            toaster.pop('error', '', rejection.data.email[0]);
                        }
                    }
                    else {
                        toaster.pop('error', '', 'Esta p\u00E1gina no existe');
                    }
                    break;
                case 400:
                    if('data' in rejection){
                        if('non_field_errors' in rejection.data){
                            if('config' in rejection){
                                if(rejection.config.url.search('/api/users/login/') != -1){
                                    show = false;
                                }
                            }
                            if(show)
                                toaster.pop('error', '', rejection.data.non_field_errors[0]);
                        }else if('email' in rejection.data){
                            toaster.pop('error', '', 'Ya existe un usuario con este correo');
                        }else if ('first_name' in rejection.data) {
                            toaster.pop('error', '', rejection.data.first_name[0]);
                        }else if ('last_name' in rejection.data) {
                            toaster.pop('error', '', rejection.data.last_name[0]);
                        }else if('picture' in rejection.data){
                            toaster.pop('error', '', rejection.data.picture[0]);
                        }else if('response' in rejection.data){
                            if('message' in rejection.data.response){
                                toaster.pop('error', '', rejection.data.response.message);
                            }else if('picture' in rejection.data.response){
                                toaster.pop('error', '', rejection.data.response.picture[0]);
                            }else {
                                toaster.pop('error', '', '400: Ha ocurrido un problema al conectarse con instapagos');
                            }
                        }else if('detail' in rejection.data){
                            toaster.pop('error', '', rejection.data.detail);
                        }
                    }else {
                        toaster.pop('error', '', '400: Ha ocurrido un problema en el servidor');
                    }
                    
                    break;
                case 401:
                    if (rejection.statusText == "Unauthorized"){
                        SessionService.destroy(null);
                        SessionService.deleteEmail();
                        SessionService.deleteId();
                        SessionService.deleteName();
                        SessionService.deleteLastname();
                        SessionService.deletePhone();
                        SessionService.deleteAddress();
                        window.location = '/';
                    }
                    if ('detail' in rejection.data) {
                        toaster.pop('error', '', rejection.data.detail);
                    }else{
                        toaster.pop('error', '', '401: Ha ocurrido un problema en el servidor');
                    }
                    break;
                case 403:
                    if ('detail' in rejection.data) {
                        toaster.pop('error', '', rejection.data.detail);
                    }else{
                        toaster.pop('error', '', '403: Ha ocurrido un problema en el servidor');
                    }
                    break;
                case 500:
                    if('detail' in rejection.data){
                        toaster.pop('warning', '', rejection.data.detail);
                    }else{
                        toaster.pop('error', '', '500: Ha ocurrido un problema en el servidor');
                    }
                    break;
                default:
                    toaster.pop('error', '', 'Disculpe, hay problemas con el servidor');
            }
            return $q.reject(rejection);
        }
    };
}]);
