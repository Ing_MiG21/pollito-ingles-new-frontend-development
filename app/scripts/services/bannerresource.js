'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.BannersResource
 * @description
 * # BannersResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('BannersResource', ['$resource', 'API', function ($resource, API) {

      return $resource(API.banners, {}, {
          getBanners: {
            url: API.banners,
            method: 'GET'  
          }
      });

}]);