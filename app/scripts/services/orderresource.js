'use strict';

angular
	.module('pollitoinglesApp')
	.factory('OrderResource', ["$resource", "API", function ($resource, API) {
		return $resource(API.orders, {}, {
      
      getTotals: {
        method: 'GET',
        url: API.orders + 'order-totals/?pid=:pid',
        params: {
          pid: '@pid'
        }
      },
      getOrders: {
        method: 'GET',
        url: API.orders + 'order-list/',
      },
      getSales: {
        method: 'GET',
        url: API.orders + 'order-list/?sid=me',
      },
      orderDetail: {
        method: 'GET',
        url: API.orders + 'order/:id_order/',
      },
      saleDetail: {
        method: 'GET',
        url: API.orders + 'order/:id_order/?sid=me',
      },
      instapagoPost: {
        method: 'POST',
        url: API.orders + 'instapago-payment/'
      },
      setOrder: {
        method: 'POST',
        url: API.orders + 'order/'
      },
      setExchange: {
        method: 'POST',
        url: API.orders + 'exchange/'
      }
    });
  }]);
