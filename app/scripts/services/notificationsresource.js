'use strict';

/**
* @ngdoc service
* @name pollitoinglesApp.NotificationsResource
* @description
* # NotificationsResource
* Factory in the pollitoinglesApp.
*/
angular.module('pollitoinglesApp')
.factory('NotificationsResource', ['$resource', 'API', function ($resource, API) {
    return $resource(API.notifications, {}, {
        get: {
            url: API.notifications,
            method: 'GET'
        },
        getFrom: {
            method: 'GET',
            url: API.notifications + '?is_read=:is_read',
            params: {
                is_read: '@is_read'
            }
        },
        update: {
            url: API.notifications + ':id/',
            method: 'PUT',
            params: {
                id: '@id'
            }
        }
    });
}]);
