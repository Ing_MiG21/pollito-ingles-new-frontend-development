'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.VariationResource
 * @description
 * # VariationResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
    .factory('VariationResource', ['$resource', 'API', function ($resource, API) {

        return $resource(API.variation, {}, {
	    	get: {
	        	method: 'GET',
	        	url: API.variation + ':id_product/:id_variation/:id_filter/',
	        	params: {
	                id_product: '@id_product', 
	                id_variation: '@id_variation', 
	                id_filter: '@id_filter'
	            }
	    	}
        });
}]);