'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.FaqResource
 * @description
 * # FaqResource
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('FaqResource', ['$resource', 'API', function ($resource, API) {

      return $resource(API.faq, {}, {
          getFAQ: {
              method: 'GET',
              url: API.faq
          }
      });
}]);