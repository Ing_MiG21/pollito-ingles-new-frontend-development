'use strict';

angular
	.module('pollitoinglesApp')
	.factory('PollitoResource', ["$resource", "API", function ($resource, API) {
		return $resource(API.about_us, {}, {
      team: {
        url: API.about_us,
        method: 'GET',
        isArray: true
      },
      contactUs: {
        url: API.about_us + "contact_us/",
        method: 'POST'
      },
      confPi: {
        url: API.about_us + "conf/",
        method: 'GET'
      }
    });
}]);
