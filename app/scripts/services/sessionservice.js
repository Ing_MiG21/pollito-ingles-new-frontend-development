'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.SessionService
 * @description
 * # SessionService
 * Factory in the pollitoinglesApp.
 */
angular.module('pollitoinglesApp')
  .factory('SessionService', function ($window) {
    // Service logic
    // ...
    var sessionService = {};

    sessionService.create = function (credentials) {
    	$window.sessionStorage.setItem('token', credentials.token);
    };

    sessionService.destroy = function (data) {
        $window.sessionStorage.removeItem('token');
    };

    sessionService.hasToken = function () {
    	return !$window.sessionStorage.getItem('token');
    };

    sessionService.getToken = function () {
    	return $window.sessionStorage.getItem('token');
    }

    sessionService.setEmail = function (email) {
        $window.sessionStorage.setItem('email', email);
    };

    sessionService.getEmail = function () {
        return $window.sessionStorage.getItem('email');
    }

    sessionService.deleteEmail = function () {
        return $window.sessionStorage.removeItem('email');
    }

    sessionService.setId = function (id) {
        $window.sessionStorage.setItem('id', id);
    };

    sessionService.getId = function () {
        return $window.sessionStorage.getItem('id');
    }

    sessionService.deleteId = function () {
        return $window.sessionStorage.removeItem('id');
    }

    sessionService.setName = function (name) {
        $window.sessionStorage.setItem('name', name);
    };

    sessionService.getName = function () {
        return $window.sessionStorage.getItem('name');
    }

    sessionService.deleteName = function () {
        return $window.sessionStorage.removeItem('name');
    }

    sessionService.setLastname = function (lastname) {
        $window.sessionStorage.setItem('lastname', lastname);
    };

    sessionService.getLastname = function () {
        return $window.sessionStorage.getItem('lastname');
    }

    sessionService.deleteLastname = function () {
        return $window.sessionStorage.removeItem('lastname');
    }

    sessionService.setPhone = function (phone) {
        $window.sessionStorage.setItem('phone', phone);
    };

    sessionService.getPhone = function () {
        return $window.sessionStorage.getItem('phone');
    }

    sessionService.deletePhone = function () {
        return $window.sessionStorage.removeItem('phone');
    }

    sessionService.setAddress = function (address) {
        $window.sessionStorage.setItem('address', address);
    };

    sessionService.getAddress = function () {
        return $window.sessionStorage.getItem('address');
    }

    sessionService.deleteAddress = function () {
        return $window.sessionStorage.removeItem('address');
    }

    sessionService.setDataComplete = function (data_complete) {
        var dc = JSON.stringify(data_complete);
        $window.sessionStorage.setItem('data_complete', dc);
    };

    sessionService.getDataComplete = function () {
        var dc = $window.sessionStorage.getItem('data_complete');
        if (dc)
            return JSON.parse(dc);
        return null;
    }

    sessionService.deleteDataComplete = function () {
        return $window.sessionStorage.removeItem('data_complete');
    }

    sessionService.setCart = function (data) {
        var dc = JSON.stringify(data);
        $window.sessionStorage.setItem('data', dc);
    };

    sessionService.getCart = function () {
        var dc = $window.sessionStorage.getItem('data');
        if(dc)
            return JSON.parse(dc);
        return null;
    }

    sessionService.deleteCart = function () {
        return $window.sessionStorage.removeItem('data');
    }

    return sessionService;
  });
