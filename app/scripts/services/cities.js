'use strict';

/**
 * @ngdoc service
 * @name pollitoinglesApp.Cities
 * @description
 * # Cities
 * Factory in the pollitoinglesApp.
 */
 angular.module('pollitoinglesApp')
  .factory('Cities', function ($http, $q) {

      //Create a class that represents our name service
      function Cities() {

          var self = this;

          self.cities = [];
          self.nextURL = '';

          //   getName returns a promise which when fulfilled returns the name
          self.getCities = function(url, param) {

              //   Create a deferred operation.
              var deferred = $q.defer();

              $http.get(url, {params: {'gid':param}}).then(function(res){
                  //Push each location to the returning array
                  for (var i = 0; i < res.data.results.length; i++) {
                      self.cities.push(res.data.results[i]);
                  }
                  //Get the next url to consult (if any)
                  self.nextURL = res.data.next;

                  /*If there is a next URL to consult and
                  it has a query string parameter*/
                  if(self.nextURL){
                      self.getCities(self.nextURL, param);
                  }

                  //Return the resolved data
                  deferred.resolve(self.cities);

              }, function(error){
                  deferred.reject(error);
              });

              //   Now return the promise.
              return deferred.promise;
          };

          self.clear = function(){
              self.cities = [];
          }
      }

      return new Cities();
  });
