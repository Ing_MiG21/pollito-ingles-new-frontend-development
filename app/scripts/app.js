'use strict';

/**
 * @ngdoc overview
 * @name pollitoinglesApp
 * @description
 * # pollitoinglesApp
 *
 * Main module of the application.
 */
angular
  .module('pollitoinglesApp', [
    '720kb.socialshare',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'toaster',
    'ui.router',
    'ngFileUpload',
    'combo-date',
    'ngImgCrop',
    'ui.bootstrap',
    'ngWebSocket',
    'blockUI',
    'rzModule',
    'masonry',
    'ng.deviceDetector'
  ])
  	.config(function($httpProvider, $resourceProvider, blockUIConfig){
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
      $resourceProvider.defaults.stripTrailingSlashes = false;
      $httpProvider.interceptors.push('sessionInjector');
      $httpProvider.interceptors.push('responseErrorInterceptor');
      blockUIConfig.message = 'Espera un poco';
      blockUIConfig.requestFilter = function(config) {
        // If the request starts with '/api/quote' ...
        if(config.url.match(/\/api\/orden-de-compra/g)) {
          return false; // ... don't block it.
        }
      };
  })
  .run(function($rootScope, $state, toaster, SessionService, $window, $templateCache, $websocket, wsUrl, facebookId){
      $rootScope.$on('$stateChangeSuccess', function() {
        $rootScope.$state = $state;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if($state.current.name.substring(0,19) == 'main.tienda-pollito') {
          $rootScope.isTPollito=true;
          $rootScope.AdvancedSearch=true;
          $rootScope.SearchByAge=true;
        }
        else $rootScope.isTPollito = false;
      });

      //Load Facebook SDK
      (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));

      $window.fbAsyncInit = function() {
          FB.init({
              appId : facebookId,
              cookie : true,
              xfbml : true,
              version : 'v2.7'
          });
      }

      $templateCache.put("uib/template/carousel/carousel.html",
        "<div ng-mouseenter=\"pause()\" ng-mouseleave=\"play()\" class=\"carousel\" ng-swipe-right=\"prev()\" ng-swipe-left=\"next()\">\n" +
        "  <div class=\"carousel-inner\" ng-transclude></div>\n" +
        "  <a role=\"button\" href=\"\" class=\"left carousel-control\" ng-click=\"$event.preventDefault();prev()\" ng-class=\"{ disabled: isPrevDisabled() }\" ng-show=\"slides.length > 1\">\n" +
        "    <span aria-hidden=\"true\" class=\"pointer-categories pointer-left\"></span>\n" +
        "    <span class=\"sr-only\">previous</span>\n" +
        "  </a>\n" +
        "  <a role=\"button\" href=\"\" class=\"right carousel-control\" ng-click=\"$event.preventDefault();next()\" ng-class=\"{ disabled: isNextDisabled() }\" ng-show=\"slides.length > 1\">\n" +
        "    <span aria-hidden=\"true\" class=\"pointer-categories pointer-right\"></span>\n" +
        "    <span class=\"sr-only\">next</span>\n" +
        "  </a>\n" +
        "  <ol class=\"carousel-indicators\" ng-show=\"slides.length > 1\">\n" +
        "    <li ng-repeat=\"slide in slides | orderBy:indexOfSlide track by $index\" ng-class=\"{ active: isActive(slide) }\" ng-click=\"select(slide)\">\n" +
        "      <span class=\"sr-only\">slide {{ $index + 1 }} of {{ slides.length }}<span ng-if=\"isActive(slide)\">, currently active</span></span>\n" +
        "    </li>\n" +
        "  </ol>\n" +
        "</div>\n" +
        "");
      
      $templateCache.put('angular-block-ui/angular-block-ui.ng.html', 
        '<div class=\"block-ui-overlay\"></div><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div class=\"block-ui-message\" ng-class=\"$_blockUiMessageClass\"><img src="images/loader.gif"><br>{{ state.message }}</div></div>');

      $rootScope.dataStream = $websocket(wsUrl+'pinotif?subscribe-broadcast');
      $rootScope.dataStream.onMessage(function(message) {
        if(message.data!='--heartbeat--'){
          var datos = JSON.parse(message.data);
          if(!SessionService.hasToken()){
            if(datos.users.indexOf(parseInt(SessionService.getId())) != -1){
              toaster.pop('success','',datos.message);
              var ntotal = $rootScope.notifications.total + 1;
              $rootScope.notifications.count = (ntotal >= 100)? '+99':ntotal;
              $rootScope.notifications.total = ntotal;
            }
          }
        }
      });
      $rootScope.dataStream.onError(function(event){
        console.log('connection Error', event);
      });
      $rootScope.dataStream.onClose(function(event){
        console.log('connection closed', event);
      });

      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
          $rootScope.$broadcast('clean-search');
          if (((toState.name == 'main.login') || (toState.name == 'main.register')) && SessionService.getId()) {
              toaster.pop('error', '', 'No tiene acceso a esta vista');
              event.preventDefault();
          }else if (((toState.name == 'main.testimonies') 
                      || (toState.name == 'main.sell') 
                      || (toState.name == 'main.checkout') 
                      || (toState.name == 'main.plan_checkout') 
                      || (toState.name == 'main.exchange')
                      || (toState.name == 'main.profile.talk')
                      || (toState.name == 'main.complete-your-registry')
                      || (toState.name == 'main.registry-basic-info')
                      || (toState.name == 'main.registry-basic-info-2')
                      || (toState.name == 'main.registry-basic-info-3')
                      || (toState.name == 'main.registry-basic-info-4')
                      || (toState.name == 'main.profile.sendmessage')) && !SessionService.getId()) {
              toaster.pop('error', '', 'Debes iniciar sesi\u00F3n');
              $state.go('main.login');
              event.preventDefault();
          }
      });

  });
