'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ProfiletabOrdersCtrl
 * @description
 * # ProfiletabOrdersCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ProfiletabOrdersCtrl', ['$state', '$scope', function ($state, $scope) {

      $scope.isActive = function (state) {
          return $state.$current.name === 'main.profile.tabs-orders.' + state;
      };     
}]);
