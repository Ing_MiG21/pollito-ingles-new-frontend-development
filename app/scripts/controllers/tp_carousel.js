'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:tp_CarouselCtrl
 * @description
 * # tp_CarouselCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('tp_CarouselCtrl', ['$scope','$rootScope', function ($scope,$rootScope) {
  	$rootScope.detailTP = false;
  }]);