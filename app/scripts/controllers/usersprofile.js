'use strict';
/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:UsersProfileCtrl
 * @description
 * # UsersProfileCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('UsersProfileCtrl', ['$scope', '$rootScope', 'toaster', '$state', 'user', 'SessionService', 'session_following', 'ParentsResource', 'reputation',
    function ($scope, $rootScope, toaster, $state, user, SessionService, session_following, ParentsResource, reputation) {

      $scope.profile = $state.current.data.profile;
      $rootScope.following = false;
      $scope.thisSession=false;
      $scope.reputation = reputation;
      $scope.me = user;
      $scope.children = $scope.me.children;
      $scope.enterprising=false;
      $scope.is_registry_complete=true;

      if(session_following){
        angular.forEach(session_following.results, function(value) { 
          if(value.id == user.pk) $rootScope.following = true;
        });        
      }

      if($scope.me.plan){
        if($scope.me.plan.code == '000002')$scope.enterprising=true;
      };

      if(!SessionService.hasToken() && user.pk == SessionService.getId()) $scope.thisSession=true;

      angular.forEach($scope.children, function(child) {
          var edad = moment(child.birthdate).fromNow();
          if (edad.includes("a year ago")) {
              child.age = edad.replace("a year ago", "1 año");
          }else if(edad.includes("years ago")){
              child.age = edad.replace("years ago", " años");
          }else if (edad.includes("months ago")) {
              child.age = edad.replace("months ago", " meses")
          }else if (edad.includes("a month ago")) {
              child.age = edad.replace("a month ago", "1 mes")
          }
      });

      if($scope.me.pk == SessionService.getId()){
          $state.go('main.profile.tabs.products');
      }else {
          $state.go('main.users.profile.tabs.products');
      }

       //   TODO use $scope.me.reputation when available
      if($scope.reputation[0].average_reputation){
          $scope.rating = $scope.reputation[0].average_reputation;
      }else {
          $scope.rating = 0;
          //falserating = 5 so that it renders 5 grey flowers in the view
          $scope.falserating = 5;
      }

      $scope.follow = function(){
        if($scope.thisSession){
            ParentsResource.follow({id_user: $scope.me.pk}, function(res){
              toaster.pop('success', '', "El usuario ha sido agregado a tu lista de seguidos");
              $rootScope.following = true;
            });
        }else{
            toaster.pop('error', '', 'Debes iniciar sesi\u00F3n');
            $state.go('main.login');
        }
      }

  }]);
