'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:SendMessageCtrl
 * @description
 * # SendMessageCtrl
 * Controller of the pollitoinglesApp
 */


angular.module('pollitoinglesApp')
  .controller('SendMessageCtrl', ['$scope', 'user', 'me', 'ParentsResource', '$http', 'toaster','API' ,
  function ($scope, user, me ,ParentsResource, $http, toaster, API) {

  	$scope.user = user;
  	$scope.me = me;

  	$scope.enviar = function(msj){

      $http.get(API.contentType)
      .then(
        function(succes)
        { 
          $scope.contentType = succes.data.content_type;
          
          var req = {
            method: 'POST',
            url: API.message_private,
            data:{ 
              user: $scope.user.pk,
              message: msj 
            }
          }
          $http(req).then(function(succes){ 
            
             toaster.pop('success', '', 'Su mensaje ha sido enviado exitosamente');
            
            $http.get(API.comments).then(function(succes){ $scope.comments = succes.data; });

          },function(error){toaster.pop('error', '', 'El Mensaje no puede estar vacío');});
        },
        function(error){console.log("content_type error");}
      );
    };
 }]);