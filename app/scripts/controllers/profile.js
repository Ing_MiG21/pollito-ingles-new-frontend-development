'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ProfileCtrl', ['$scope', 'me', '$state', '$rootScope', 'SessionService', 'reputation', '$http','API','toaster', 'check', 
  function ($scope, me, $state, $rootScope, SessionService, reputation, $http, API, toaster, check) {

      $scope.me = me;
      $scope.children = me.children;
      $scope.reputation = reputation;
      $scope.name = $rootScope.UserName.includes("@")? SessionService.getEmail():$rootScope.UserName;
      $scope.enterprising=false;
      $scope.thisSession=false;
      console.log(check);
      $scope.is_registry_complete = check.success;

      if($scope.me.plan){
        if($scope.me.plan.code == '000002')$scope.enterprising=true;
      };

      if(!SessionService.hasToken() && $scope.me.pk == SessionService.getId()) $scope.thisSession=true;
      
      angular.forEach($scope.children, function(child) {
          var edad = moment(child.birthdate).fromNow();
          if (edad.includes("a year ago")) {
              child.age = edad.replace("a year ago", "1 a\u00F1o");
          }else if(edad.includes("years ago")){
              child.age = edad.replace("years ago", " a\u00F1os");
          }else if (edad.includes("months ago")) {
              child.age = edad.replace("months ago", " meses")
          }else if (edad.includes("a month ago")) {
              child.age = edad.replace("a month ago", "1 mes")
          }
      });

      $scope.enviar = function(msj){

      $http.get(API.contentType)
      .then(
        function(succes)
        { 
          $scope.contentType = succes.data.content_type;
          
          var req = {
            method: 'POST',
            url: API.message_private,
            data:{ 
              user: $scope.me.pk,
              message: msj 
            }
          }
          $http(req).then(function(succes){ 
            
             toaster.pop('success', '', 'Su mensaje ha sido enviado exitosamente');
            
            $http.get(API.comments).then(function(succes){ $scope.comments = succes.data; });

          },function(error){toaster.pop('error', '', 'El Mensaje no puede estar vacío');});
        },
        function(error){console.log("content_type error");}
      );
    };

    //   TODO use $scope.me.reputation when available
      if($scope.reputation[0].average_reputation){
          $scope.rating = $scope.reputation[0].average_reputation;
      }else {
          $scope.rating = 0;
          //falserating = 5 so that it renders 5 grey flowers in the view
          $scope.falserating = 5;
      }

      //This is so the button Mi Perfil is shown in the profile view
      if(  
           $state.is('main.profile.not-mess.notifications') 
        || $state.is('main.profile.tabs-orders.orders')
        || $state.is('main.profile.tabs-orders.sales')
        || $state.is('main.profile.tabs-orders.exchange')
        )
      {
        $scope.notify = true;
      }

      //This is to show/hide buttons in profile view accordingly
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
          if(toState.name == 'main.profile.tabs.products'){
              $scope.notify = false;
          }else if (toState.name == 'main.profile.not-mess.notifications' || toState.name == 'main.profile.tabs-orders.orders') {
              $scope.notify = true;
          }
      });
  }]);
