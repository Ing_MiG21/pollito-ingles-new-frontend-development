'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:OrderDetailCtrl
 * @description
 * # OrderDetailCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('OrderDetailCtrl', ['$scope', 'order',function ($scope, order){
  	$scope.order=order;

	angular.forEach(order.associated_reputation, function(reputation){
	    if(reputation.seller_or_buyer == 1) order.reputation=true;
	});
	
}]);
