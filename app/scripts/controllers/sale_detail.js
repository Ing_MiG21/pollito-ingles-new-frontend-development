'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:SaleDetailCtrl
 * @description
 * # SaleDetailCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('SaleDetailCtrl', ['$scope', 'sale',function ($scope, sale){
  	$scope.sale=sale;

	angular.forEach(sale.associated_reputation, function(reputation){
	    if(reputation.seller_or_buyer == 2) sale.reputation=true;
	});
  	
}]);
