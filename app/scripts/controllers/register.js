'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:registerCtrl
 * @description
 * # registerCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('registerCtrl',['$rootScope', '$scope', '$http', 'API', 'AuthService', 'SessionService', 'NotificationsResource', 'UserResource', 'toaster', '$state', '$anchorScroll', '$location', 'Regex','$uibModal',
  function ($rootScope, $scope, $http, API, AuthService, SessionService, NotificationsResource, UserResource, toaster, $state, $anchorScroll, $location, Regex, $uibModal) {

      //Regex for phone
      $scope.regex = Regex.cellphone;
      //Regex for names and surnames
      $scope.regex2 = Regex.names;
      $scope.user = {};
      $scope.userIsConnected = false;
      $scope.fb_user = {};
      $scope.is_fb_login = false;

      $location.hash('scrollHere');
      $anchorScroll();

    //   TODO delete all logs here

      $scope.registerForm = function(){
          if ($scope.newUserForm.$valid) {
              $scope.register();
          }
      }

      $scope.acceptTerms = function(){
          var modalInstance = $uibModal.open({
              templateUrl: 'views/register-modal.html',
              controller: 'ModalCtrl',
              size: 'lg'
          });

          modalInstance.result.then(function () {
              $scope.user.terms_conditions = true;
          }, function (){
          });
      }

      $scope.intentLogin = function() {

          FB.getLoginStatus(function(response) {
              if (response.status == 'connected') {
                  //Disconnect and connect again to ask for permissions (email)
                  $scope.logout();
                  $scope.login();
              }else if (response.status === 'not_authorized') {
                  $scope.login();
              }else {
              }
          });

          if(!$scope.userIsConnected) {
              $scope.login();
          }
      };

      $scope.login = function() {
          FB.login(function(response) {
              if (response.status == 'connected') {
                  $scope.userIsConnected = true;
                  $scope.info();
              }
          }, {scope: 'public_profile, email'});
      };

      $scope.info = function() {
          FB.api('/me', {fields: 'id,name,email'}, function(response) {
              $scope.$apply(function() {
                  $scope.me = response;
                  $scope.fb_user.email = response.email;
                  $scope.fb_user.id_facebook = response.id;
                  $scope.fb_user.terms_conditions = 1;
                  $scope.is_fb_login = true;
              });
              $scope.register();
          });
      };

      $scope.logout = function() {
          FB.getLoginStatus(function(response) {
              if (response.status == 'connected') {
                  FB.logout(function() {
                      $scope.$apply(function() {
                          $scope.user = {};
                          $scope.userIsConnected = false;
                      });
                  });
              }
          });
      }

      $scope.register = function(){
          var data_login = ($scope.is_fb_login) ? $scope.fb_user : $scope.user;
          UserResource.create(data_login, function(res){
            if($scope.is_fb_login){
              $scope.initLogin();
            }else{
              toaster.pop('success', '', 'Acabas de registrarte exitosamente');
              toaster.pop('success', '', 'Dirígete a tu correo y activa tu cuenta con el mensaje que te hemos enviado');
              $state.go('main.login');
            }
          });
      }

      $scope.initLogin = function(){
          var data_login = { username: $scope.fb_user.email, password: $scope.fb_user.id_facebook }
          AuthService.login(data_login).then(function(res){

              toaster.pop('success', '', 'Bienvenido a Pollito Ingl\u00E9s');

              var email = SessionService.getEmail();
              UserResource.getUserByEmail({email:email}, function(res){
                
                var me = res;
                SessionService.setId(me.pk);
                SessionService.setName(me.parent.first_name);
                SessionService.setLastname(me.parent.last_name);
                SessionService.setPhone(me.parent.phone_number);
                SessionService.setAddress(me.parent.direction);
                var name = SessionService.getName() + ' ' + SessionService.getLastname();
                $rootScope.UserName = name == ' '? SessionService.getEmail():name;

                var notifications = NotificationsResource.get(function(res){return res});
                notifications.$promise.then(function(res){$rootScope.notifications = res});
                $state.go('main.home');
              });
          });
      }

  }]);
