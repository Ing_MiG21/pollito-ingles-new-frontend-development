'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pollitoinglesApp
 */

angular.module('pollitoinglesApp')
  .controller('MainCtrl', ['$rootScope', '$scope', 'PollitoResource', 'deviceDetector', '$websocket', 
                'AuthService', 'toaster', '$state', 'SessionService', 
                'NotificationsResource', 'ParentsResource', 'SearchResource', 
                '$anchorScroll', '$location', '$filter', 'AdvancedSearchResource',
  function ($rootScope, $scope, PollitoResource, deviceDetector, $websocket, 
            AuthService, toaster, $state, SessionService, 
            NotificationsResource, ParentsResource, SearchResource, 
            $anchorScroll, $location, $filter, AdvancedSearchResource) {
    
    $rootScope.incentives=false;
    $rootScope.AdvancedSearch=true;
    $rootScope.SearchByAge=true;
    $scope.links_rrss = {
        facebook_link: '#',
        twitter_link: '#',
        pinterest_link: '#',
        instagram_link: '#',
        address_pollito: '-',
        email_pollito: '-',
        phone_pollito: '-'
    };

    if(deviceDetector.os == 'mac' || deviceDetector.browser == 'safari'){
        $rootScope.isMac = true;
    }

    PollitoResource.confPi(function(rrss){
        for (var i = 0; i < rrss.results.length; i++) {
            if($scope.links_rrss[rrss.results[i]['key']]){
                $scope.links_rrss[rrss.results[i]['key']] = rrss.results[i]['value'];
            }
        };
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if(toState.name == 'main.terms-conditions' || 
            toState.name == 'main.copyright-policies' || 
            toState.name == 'main.privacy-policy' || 
            toState.name == 'main.acceptable-use-policy' || 
            toState.name == 'main.login' || 
            toState.name == 'main.generalsearch' || 
            toState.name == 'main.about-us' || 
            toState.name == 'main.home' || 
            toState.name == 'main.categories'){
            // $location.hash('scrollHere');
            $anchorScroll('scrollHere');
        }else if(toState.name == 'main.generalsearch'){
            // TODO fix this horrible hack, prevent URL to become polluted
            var old = $location.hash();
            $anchorScroll('scrollHere');
            $location.hash(old);
        }
    });

    $scope.exit = function () {
    	AuthService.logout().then(function (resp) {
            toaster.pop('success', '', 'Cerraste sesi\u00F3n exitosamente');
            $state.go('main.login');
    	});
    }

    $scope.hasToken = function(){
        return SessionService.hasToken();
    };

    $scope.ShowSearchAge = function(){
        $rootScope.SearchByAge=!$rootScope.SearchByAge;
        $rootScope.AdvancedSearch=true;
    };

    $scope.goAdvancedSearch = function() {
        $rootScope.AdvancedSearch=!$rootScope.AdvancedSearch;
        $rootScope.SearchByAge=true;       
    }

    $scope.hideAll = function(){
        $rootScope.AdvancedSearch=true;
        $rootScope.SearchByAge=true; 
    }

    $scope.go = function(){
        if(SessionService.getEmail()){
            $state.go('main.profile.not-mess.notifications');
        }else {
            toaster.pop('error', '', 'Debes iniciar sesi\u00F3n');
        }
    };

    $scope.goIncentives = function(){
        $state.go('main.incentivos');
    };

    $scope.search = function(str){  
        if($scope.query == null || $scope.query == '') 
        {
            toaster.pop('error', '', 'Debe indicar un criterio de b\u00FAsqueda');
            return false;    
        }
        var data = { query: $scope.query };
        data['is_chick'] = ($rootScope.isTPollito) ? 1:0;
        $state.go('main.generalsearch', data);
    }

    $scope.$on('clean-search', function(evt){
        evt.preventDefault();
        $scope.query = '';
    })

    $scope.isTPHref = function () {
        if($rootScope.isTPollito) return $state.href('main.tienda-pollito.carousel');
        else return $state.href('main.home'); 
    }

    $scope.isTPHref2 = function () {
        if($rootScope.isTPollito) return  $state.href('main.home'); 
        else return $state.href('main.tienda-pollito.carousel');
    }

    if(!SessionService.hasToken())
    {
        $rootScope.notifications = {};
        NotificationsResource.getFrom({is_read:1}, function(res){
            $rootScope.notifications.count = (res.count >= 100) ? '+99':res.count;
            $rootScope.notifications.total = res.count;
        });

        $rootScope.incentives=true;
        ParentsResource.getRewards(function(res){
            $rootScope.totalEggs = 0;
            angular.forEach(res.results, function(value, key) {
                $rootScope.totalEggs += parseInt(value.reward_point);
            });
        });
    }
    var name = SessionService.getName() + ' ' + SessionService.getLastname();
    $rootScope.UserName = name == ' '? SessionService.getEmail():name;

}]);
