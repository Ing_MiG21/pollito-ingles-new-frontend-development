'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:PlanCheckoutCtrl
 * @description
 * # PlanCheckoutCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('PlanCheckoutCtrl', ['$rootScope', '$sce','$scope', 'totals', 'plan', 'MembershipsResource', 'OrderResource', 'ParentsResource', 'toaster', '$state', 'SessionService', '$stateParams', '$location', '$anchorScroll',
  function ($rootScope, $sce, $scope, totals, plan, MembershipsResource, OrderResource, ParentsResource, toaster, $state, SessionService, $stateParams, $location, $anchorScroll) {

      $scope.totals = totals.totals;
      $scope.plan = plan;
      $scope.title_plan = $scope.plan.plan_name.split(" ");
      $scope.tdcResponse = {};
      $scope.tdcVoucher = '';
      $scope.isSubmited = false;
      $scope.displayLoader = true;
      $scope.showConfirm = false;
      $scope.url = $location.absUrl();
      $scope.years = [];
      $scope.Math = window.Math;
      $scope.total_discount = null;
      var ckd = angular.copy(totals.totals);
      $scope.order_shipping = {};
      $scope.tdc = {};

      for (var i = 0; i <= 10; i++) {
          $scope.years.push({value:moment().add(i,'year').year(), text:moment().add(i,'year').year()});
      }

      $scope.months = [];
      $scope.months.push({value:'01', text:'Enero'});
      $scope.months.push({value:'02', text:'Febrero'});
      $scope.months.push({value:'03', text:'Marzo'});
      $scope.months.push({value:'04', text:'Abril'});
      $scope.months.push({value:'05', text:'Mayo'});
      $scope.months.push({value:'06', text:'Junio'});
      $scope.months.push({value:'07', text:'Julio'});
      $scope.months.push({value:'08', text:'Agosto'});
      $scope.months.push({value:'09', text:'Septiembre'});
      $scope.months.push({value:'10', text:'Octubre'});
      $scope.months.push({value:'11', text:'Noviembre'});
      $scope.months.push({value:'12', text:'Diciembre'});

      $scope.mine = function(pub){
          var id = SessionService.getId();
          if(id == pub.user.pk){
              return true
          }else if (id === null) {
              return false
          }
      }

      $scope.type_plan = function(code){
        if(code == '000001'){
          return 'images/plan-mp.png';
        }else if(code == '000002'){
          return 'images/plan-me.png';
        }

        return 'images/plan-b.png';
      }

      $scope.sendPayment = function(){
        if ($scope.tdcForm.$valid) {
          $location.hash('scrollHere');
          $anchorScroll();
          $scope.tdc.total_amount = $scope.totals.total;
          $scope.showConfirm = true;
          $scope.isSubmited = true;
          $scope.displayLoader = true;
          console.log($scope.totals);

          OrderResource.instapagoPost($scope.tdc, function(res){
            if(res.status == 400){
              $scope.isSubmited = false;
            }else{
              //lanzo la creacion de la orden
              $scope.tdcResponse = res.response;
              $scope.tdcVoucher = res.response.voucher;

              var dataOrder = {
                "type_plan": $scope.plan.id,
              }

              console.log(dataOrder);

              MembershipsResource.subscription(dataOrder, function(res){
                $scope.displayLoader = false;
                toaster.pop('success', '', 'Operación realizada exitosamente');
                ParentsResource.getRewards(function(res){
                    $rootScope.totalEggs = 0;
                    angular.forEach(res.results, function(value, key) {
                        $rootScope.totalEggs += parseInt(value.reward_point);
                    });
                });
              });
            }
          });
        }
      }

      $scope.printDiv = function(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var popupWin = window.open('', '_blank', 'width=800,height=700');
        popupWin.document.open();
        popupWin.document.write(
          '<html>'+
          '<head>'+
          '<link rel="stylesheet" href="bower_components/ng-img-crop/compile/minified/ng-img-crop.css" />'+
          '<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" />'+
          '<link rel="stylesheet" href="bower_components/AngularJS-Toaster/toaster.css" />'+
          '<link rel="stylesheet" href="bower_components/angularjs-slider/dist/rzslider.css" />'+
          '<link rel="stylesheet" href="styles/main.css">'+
          '</head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
      }

  }])
  .filter('trusted', ['$sce', function($sce) {
      var div = document.createElement('div');
      return function(text) {
          div.innerHTML = text;
          return $sce.trustAsHtml(div.textContent);
      };
  }]);
