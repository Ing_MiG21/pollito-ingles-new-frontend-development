'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:TalkMessageCtrl
 * @description
 * # TalkMessageCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('TalkMessageCtrl', ['$scope', '$rootScope', '$stateParams', 'toaster', 'messages', 'me', 'ParentsResource', 'SessionService', '$http',
  function ($scope, $rootScope, $stateParams, toaster, messages, me, ParentsResource, SessionService, $http) {

    $scope.nextLink = messages.next;
    $scope.messages = messages.results;
    $scope.me = SessionService.getId();
    $scope.curr = me;
    
    $scope.next = function(){
      if($scope.nextLink){
          $http.get($scope.nextLink).then(function(res){
            for (var i = 0; i < res.data.results.length; i++) {
              $scope.messages.push(res.data.results[i]);
            }
            $scope.nextLink  = res.data.next;
          });
      }
    }

    $scope.sendMsg = function(){
      if($scope.messageForm.$valid){
        ParentsResource.postMessages({
          user: $stateParams.user,
          message: $scope.newComment
        },function(res){
          $scope.newComment = '';
          toaster.pop('success', '', 'Mensaje enviado existosamente');
          ParentsResource.getMessages({uid:$stateParams.user}, function(res){
            $scope.messages = res.results;
            $scope.nextMessages = res.next;
          });
        });
      }else{
        toaster.pop('error', '', 'Debe escribir un mesanje');
      }
    }

    //Mostrar listado de usuarios q me escribieron, conversacion
    //Mostrar detalle de la conversacion
    //Como usuario visitante debo poder escribirle al usuario en su perfil

  }]);
