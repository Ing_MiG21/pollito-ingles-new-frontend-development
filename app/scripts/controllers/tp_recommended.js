'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:TpRecommendedCtrl
 * @description
 * # TpRecommendedCtrl
 * Controller of the pollitoinglesApp
 */

angular.module('pollitoinglesApp')
  .controller('TpRecommendedCtrl', ['$scope','$anchorScroll','featured', 
  	function ($scope, $anchorScroll, featured){

		$scope.featured = featured.results;

		$scope.scroll = function(){
			$anchorScroll();
		}
}]);
