'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:PlansCtrl
 * @description
 * # PlansCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('PlansCtrl', 
  ['$scope', 'planes',
  function ($scope, planes) {
      $scope.getPlan = function(code){
        var r = _.findIndex(planes.results, function(child){
            return (child.code == code );
        });

        return planes.results[r].id;
      }
  }]);
