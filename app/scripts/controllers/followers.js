'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:FollowersCtrl
 * @description
 * # FollowersCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('FollowersCtrl', ['$scope','SessionService', 'followers', 'following', 'ParentsResource', '$http', 'toaster',
  function ($scope, SessionService, followers, following, ParentsResource, $http, toaster) {

      $scope.followers = followers.results;
      $scope.nextLink = followers.next;
      $scope.following = false;
      $scope.loading = false;

      //This function marks users I am already following
      function IamFollowing(){
            angular.forEach(followers.results, function(follower){
                  angular.forEach(following.results, function(followMe){
                        if(follower.id == followMe.id) follower.IamFollowing=true;
                  });   
            });
      }
      IamFollowing();


      $scope.follow = function(index){
            ParentsResource.follow({id_user: $scope.followers[index].user}, function(res){
                  toaster.pop('success', '', "El usuario ha sido agregado a tu lista de seguidos");
                  $scope.followers[index].IamFollowing=true;
            });
      }

      //This function fetches the followers
      $scope.more = function(){
          if($scope.nextLink){
              $scope.loading = true;
              $http.get($scope.nextLink).then(function(res){
                  for (var i = 0; i < res.data.results.length; i++) {
                      $scope.followers.push(res.data.results[i]);
                  }
                  $scope.nextLink = res.data.next;
                  $scope.loading = false;
              });
          }
      }

  }]);
