'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ChildrenCtrl
 * @description
 * # ChildrenCtrl
 * Controller of the pollitoinglesApp
 */

  angular.module('pollitoinglesApp')
    .controller('ChildrenCtrl', ['$rootScope', '$scope', 'toaster', '$stateParams', 'Upload', 'API', 'ParentsResource', 'Regex', 'SessionService', '$state', 'me', '$uibModal',
    function ($rootScope, $scope, toaster, $stateParams, Upload, API, ParentsResource, Regex, SessionService, $state, me, $uibModal) {

        $scope.me = me;
        $scope.regex = Regex.names;
        $scope.children=angular.copy(me.children);
        $scope.numberChildren = $scope.children.length;
        $scope.yearAllowed = moment().subtract(12, 'years').year();        

        $scope.addChild = function(){

            if($scope.check() == -1){
                var child = {
                    "parent": SessionService.getId(),
                    "birthdate": moment($scope.child.birthdate).format('YYYY-MM-DD'),
                    "name": $scope.child.name,
                    "surname":$scope.child.surname,
                    "gender": $scope.child.gender
                };
                ParentsResource.addChild(child, function(res){
                    toaster.pop('success', '', 'Acabas de agregar los datos de tu hijo(a)');
                    $state.reload('main.children');
                });
            }else {
                toaster.pop('warning', '', 'Ya tienes un hijo con estos datos');
            }
        }

        $scope.updateChild = function(index){

            var myChild = $scope.children[index];

            var child = {
                "name": myChild.name,
                "surname": myChild.surname,
                "birthdate": moment(myChild.birthdate).format('YYYY-MM-DD'),
                "gender": myChild.gender,
                "parent": myChild.pk,
                "pk": SessionService.getId()
            };

            ParentsResource.updateChild({"id": myChild.pk}, child, function(res){
                toaster.pop('success', '', 'Acabas de actualizar los datos de tu hijo(a)');
                $state.reload('main.children');
            });
        }

        $scope.deleteChild = function(){

            var myChild = $scope.children[$scope.index];

            ParentsResource.deleteChild({"id": myChild.pk}, function(res){
                $scope.children.splice($scope.index, 1);
                $scope.numberChildren--;
                toaster.pop('success', '', 'Acabas de eliminar los datos de tu hij@');
                $state.reload('main.children');
            });
        }

        $scope.delete = function(index){
            $scope.index = index;

            //Create delete children modal instance
            var modalInstance = $uibModal.open({
                templateUrl: 'views/children-modal.html',
                controller: 'ModalCtrl'
            });

            /*On Accept call deleteChild()
            else just dismiss
            */
            modalInstance.result.then(function (deleteChild) {
                if(deleteChild){
                    $scope.deleteChild();
                }
            }, function (){
                console.log('Modal dismissed at: ' + new Date());
            });
        }

        $scope.check = function(){
            return _.findIndex($scope.children, function(child){
                return ($scope.child.name == child.name && moment($scope.child.birthdate).format('YYYY-MM-DD') == child.birthdate)
            });
        }

        $scope.restart = function(index){
            $scope.children[index]=angular.copy(me.children[index]);
        }
    }
  ]);
