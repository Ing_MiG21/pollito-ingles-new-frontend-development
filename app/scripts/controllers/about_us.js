'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:nosotrosCtrl
 * @description
 * # SendMessageCtrl
 * Controller of the pollitoinglesApp
 */


angular.module('pollitoinglesApp')
  .controller('nosotrosCtrl', ['$scope',  'users', 
  function ($scope, users) {

    $scope.usersall = users;

    $scope.data = [];
    // var len = $scope.usersall.length;

    
      $scope.usersall.forEach(function (arrayElem){ 
        $scope.data.push({
          name: arrayElem.name,
          description: arrayElem.description,
          picture: arrayElem.picture
        });
      });
      
    



 }]);