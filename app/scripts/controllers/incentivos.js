'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:IncentivosCtrl
 * @description
 * # IncentivosCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('IncentivosCtrl', ['$state', '$scope', 'rewards', 'incentivos', 'SessionService', function ($state, $scope, rewards, incentivos, SessionService) {
  	$scope.incentivos = incentivos;
  	$scope.no_loged = SessionService.hasToken();
  	$scope.totalEggs = 0;
    console.log(rewards);

  	if(rewards != null){
  		angular.forEach(rewards.results, function(value, key) {
			 $scope.totalEggs += parseInt(value.reward_point);
		  });
  	}

  	$scope.is_user_available = function(code){
  		var part = code.split("|");
  		if(rewards != null){
  			for (var i = 0; i < rewards.results.length; i++) {
  				if(part.length > 1){
				    if(rewards.results[i].reward.code == part[0]){ console.log("plan 1 " + part[0]);
				    	return 'images/check-egg-alt.png';
            }
            else if(rewards.results[i].reward.code == part[1]){ console.log("plan 2 " + part[1]);
              return 'images/check-egg-alt.png';
            }
  				}else{
  					if(rewards.results[i].reward.code === part[0]){
  				    	return 'images/check-egg-alt.png';
  					}
  				}
  			}
  		}

  		return 'images/check-p.png';
  	}
}]);