'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ExchangeDetailCtrl
 * @description
 * # ExchangeDetailCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ExchangeDetailCtrl', ['$scope',function ($scope){
  	$scope.exchange=
  	{
  		'amount':'3',
		'years':
		[
			{'year':'2014'},
			{'year':'2015'},
			{'year':'2016'}
		],
		'id':1,
		'title':'Zapatos Unixes RS21-Usados',
		'number':'002-989894568459-294555',
		'date':'01/04/2016',
		'nameVendor':'Judith Ayala',
		'emailVendor':'Pepito@gmail.com',
		'phoneVendor':'0426-4782123',
		'payMethod':'Intercambio',
		'myName':'Vanessa Marcano',
		'address':'Lorem ipsum dolor sit amet,consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
		'total':'72.000,00'
	}
}]);
