'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('UsersCtrl', ['$scope', 'users', 'SessionService', function ($scope, users, SessionService) {

      var email = SessionService.getEmail();

      _.remove(users, function(user){
          return user.email == email
      });
      
      $scope.users = users;

  }]);
