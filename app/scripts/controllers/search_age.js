'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:SearchAgeCtrl
 * @description
 * # SearchAgeCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
	.controller('SearchAgeCtrl', ['$scope', '$rootScope', 'toaster', '$state', 'AdvancedSearchResource', 'ShareInfoService',
	function ($scope, $rootScope, toaster, $state, AdvancedSearchResource, ShareInfoService) {
		$scope.searchAge = function(age){
			console.log("buscando por:" + age);
			var data = {
	            "look_at"       : '0',
	            "age"           : age,
	            "gender"        : '0',
	            "reputation"    : '0',
	            "city"          : '0',
	            "price"         : "0-999999999"
	        }

	        AdvancedSearchResource.query(data, function(resultSet) {
	            if(resultSet.length > 0) 
	            {
	                $scope.resultSet = resultSet;
	                ShareInfoService.setProperty($scope.resultSet);
	                
	                if($state.current.name == 'main.advanced-search-results')
	                    $state.reload('main.advanced-search-results');
	                else
	                    $state.go('main.advanced-search-results');
	            }
	            else
	            {
	                toaster.pop('warning', '', 'B\u00FAsqueda sin resultados');
	            }
	        });
    	};
	}]);
