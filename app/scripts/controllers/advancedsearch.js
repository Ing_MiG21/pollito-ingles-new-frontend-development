'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:AdvancedsearchCtrl
 * @description
 * # AdvancedsearchCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('AdvancedsearchCtrl', ['$scope', '$filter', '$location', '$anchorScroll', '$state', 'toaster', 'AdvancedSearchResource', 'ShareInfoService', 'regions', 
    function ($scope, $filter, $location , $anchorScroll, $state, toaster, AdvancedSearchResource, ShareInfoService, regions) {

    /*
    Logic for Advanced Search
    */
    $scope.search = {};
    $scope.search.age = [];

    // toggle selection for a given fruit by name
    $scope.toggleSelection = function toggleSelection(age) {
        var idx = $scope.search.age.indexOf(age);
        // is currently selected
        if (idx > -1) {
            $scope.search.age.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.search.age.push(age);
        }
        console.log($scope.search.age.join(" "));
    };
    
    $scope.slider = {
      minValue: 0,
      maxValue: 500000,
      currency: 'Bs',
      options: {
        floor: 0,
        step: 1000,
        ceil: 500000,
        minRange: 1000,
        pushRange: true,
        translate: function(value, sliderId, label) {
          switch (label) {
            case 'model':
              return '<b>Min:</b> ' + $filter('currency')(value, "Bs ", 0);
            case 'high':
              return '<b>Max:</b>  ' + $filter('currency')(value, "Bs ", 0); //+ value;
            default:
              //return 'Bs ' + value
              //$filter('currency')(value, "Bs ", 0);
              return ''; 
          }
        }
      }
    };

    $scope.regions = regions.results;

    $scope.getAdvancedSearchValues = function(){
        var data = {
            "look_at"       : $scope.search.look_at == undefined ? '0': $scope.search.look_at,
            "age"           : ($scope.search.age === undefined || $scope.search.age.length <= 0) ? '0': $scope.search.age,
            "gender"        : $scope.search.gender === undefined ? '0': $scope.search.gender,
            "reputation"    : ($scope.search.reputation === undefined || $scope.search.reputation === null) ? '0': $scope.search.reputation,
            "city"          : $scope.search.city === undefined ? '0': $scope.search.city.name,
            "price"         : $scope.slider.minValue + "-" + $scope.slider.maxValue
        }

        console.log(data);

        AdvancedSearchResource.query(data, function(resultSet) {
            if(resultSet.length > 0) 
            {
                $scope.resultSet = resultSet;
                ShareInfoService.setProperty($scope.resultSet);
                
                /*$scope.search.look_at = null;
                $scope.search.age = [];
                $scope.search.gender = null;
                $scope.search.reputation = null;
                $scope.search.city = undefined;*/
                if($state.current.name == 'main.advanced-search-results')
                    $state.reload('main.advanced-search-results');
                else
                    $state.go('main.advanced-search-results');
            }
            else
            {
                toaster.pop('warning', '', 'B\u00FAsqueda sin resultados');
            }
        });
    }

    $scope.checkValidityAdvancedSearch = function() {
        if($scope.search.look_at === undefined && $scope.search.age === undefined && $scope.search.gender === undefined && $scope.search.reputation === undefined && $scope.search.city === undefined) {            
            toaster.pop('warning', '', 'Debe indicar al menos un criterio de b\u00FAsqueda');
            return false;
        }
        return true;
    }

    /*
    End advanced search
    */    

  }]);
