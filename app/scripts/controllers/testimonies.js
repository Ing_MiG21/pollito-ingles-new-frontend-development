'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:TestimoniesCtrl
 * @description
 * # TestimoniesCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('TestimoniesCtrl', ['$scope', '$http', 'testimonies', /*'TestimoniesResource',*/ 'toaster', '$rootScope', 'SessionService',
  function ($scope, $http, testimonies, /*TestimoniesResource,*/ toaster, $rootScope, SessionService) {

    $scope.name = $rootScope.UserName.includes("@")? SessionService.getEmail():$rootScope.UserName;
    $scope.testimonials = testimonies.results;
    $scope.next=testimonies.next;

    $scope.funNext = function(){
        if($scope.next){
            $http.get($scope.next).then(function(res){
            for (var i = 0; i < res.data.results.length; i++) {
              $scope.testimonials.push(res.data.results[i]);
            }
            $scope.next = res.data.next;
        })}
    }

/*        $scope.testimony = {
            'name': '',
            'title': '',
            'content': ''
        }

        $scope.addTestimony = function(){
            TestimoniesResource.save($scope.testimony, function(res){
                toaster.pop('success', '', 'Testimonio enviado');
                $scope.testimony = '';
            });
        }
*/

}]);
