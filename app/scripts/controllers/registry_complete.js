'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:nosotrosCtrl
 * @description
 * # SendMessageCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('RegistryBasicStep1Ctrl', ['$scope',  'SessionService', 'income', 'toaster', '$state',
  function ($scope, SessionService, income, toaster, $state) {
    
    $scope.income = income.results;
    var data_step_one = SessionService.getDataComplete();
    if(data_step_one != null){
      if('education' in data_step_one && data_step_one.education!=5){
        delete data_step_one["education_text"];
      }
      if('work' in data_step_one && data_step_one.work!=6){
        delete data_step_one["work_text"];
      }
    }
    $scope.rgcomplete = (data_step_one) ? data_step_one : {};

    $scope.continue = function(){
      if($scope.validateForm()){
        SessionService.setDataComplete($scope.rgcomplete);
        $state.go('main.registry-basic-info-2');
      }
    }

    $scope.validateForm = function(){
      if($scope.stepOneForm.age_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar un rango de edad');
      }
      else if($scope.stepOneForm.degree_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar un nivel educativo');
      }
      else if($scope.rgcomplete.education == 5 && !$scope.rgcomplete.education_text){
        toaster.pop('warning', '', 'Debes especificar tu nivel educativo');
      }
      else if($scope.stepOneForm.income_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar un rango de ingresos');
      }
      else if($scope.stepOneForm.job_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar un trabajo');
      }
      else if($scope.rgcomplete.work == 6 && !$scope.rgcomplete.work_text){
        toaster.pop('warning', '', 'Coloca tu trabajo actual');
      }
      else{
        return true;
      }

      return false;
    }

  }])
  .controller('RegistryBasicStep2Ctrl', ['$scope',  'SessionService', 'toaster', '$state',
  function ($scope, SessionService, toaster, $state) {
    var data_step_two = SessionService.getDataComplete();
    $scope.rgcomplete2 = (data_step_two) ? data_step_two : {};

    $scope.continue2 = function(){
      if($scope.validateFormTwo()){
        var session_data = SessionService.getDataComplete();
        if('internet' in $scope.rgcomplete2 && $scope.rgcomplete2.internet != 4){
          delete $scope.rgcomplete2['internet_text'];
        }
        if('internet' in session_data && session_data.internet != 4){
          delete session_data['internet_text'];
        }
        if('transport' in $scope.rgcomplete2 && $scope.rgcomplete2.transport != 4){
          delete $scope.rgcomplete2['transport_text'];
        }
        if('transport' in session_data && session_data.transport != 4){
          delete session_data['transport_text'];
        }
        session_data = angular.extend(session_data, $scope.rgcomplete2);
        SessionService.setDataComplete(session_data);
        $state.go('main.registry-basic-info-3');
      }
    };

    $scope.validateFormTwo = function(){
      if($scope.stepTwoForm.con_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar una opción de conexión');
      }
      else if($scope.rgcomplete2.internet == 4 && !$scope.rgcomplete2.internet_text){
        toaster.pop('warning', '', 'Debes especificar tu medio de conexión a internet');
      }
      else if($scope.stepTwoForm.com_media_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar un equipo de conexión');
      }
      else if($scope.stepTwoForm.transport_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar un medio de transporte');
      }
      else if($scope.rgcomplete2.transport == 4 && !$scope.rgcomplete2.transport_text){
        toaster.pop('warning', '', 'Debes especificar tu medio de transporte');
      }
      else{
        return true;
      }

      return false;
    };
  }])
  .controller('RegistryBasicStep3Ctrl', ['$scope',  'SessionService', 'toaster', '$state',
  function ($scope, SessionService, toaster, $state) {
    console.log(SessionService.getDataComplete());
    var data_step_three = SessionService.getDataComplete();
    $scope.rgcomplete3 = (data_step_three) ? data_step_three : {};

    $scope.continue3 = function(){
      if($scope.validateFormThree()){
        var session_data = SessionService.getDataComplete();
        if('diapers' in $scope.rgcomplete3 && $scope.rgcomplete3.diapers != 4){
          delete $scope.rgcomplete3['diapers_eco_text'];
        }
        if('diapers' in session_data && session_data.diapers != 4){
          delete session_data['diapers_eco_text'];
        }
        if('diapers' in $scope.rgcomplete3 && $scope.rgcomplete3.diapers != 5){
          delete $scope.rgcomplete3['diapers_text'];
        }
        if('diapers' in session_data && session_data.diapers != 5){
          delete session_data['diapers_text'];
        }
        if('milk' in $scope.rgcomplete3 && $scope.rgcomplete3.milk != 12){
          delete $scope.rgcomplete3['milk_text'];
        }
        if('milk' in session_data && session_data.milk != 12){
          delete session_data['milk_text'];
        }
        session_data = angular.extend(session_data, $scope.rgcomplete3);
        SessionService.setDataComplete(session_data);
        $state.go('main.registry-basic-info-4');
      }
    };

    $scope.validateFormThree = function(){
      if($scope.stepThreeForm.diapers_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar una marca de pañales');
      }
      else if($scope.rgcomplete3.diapers == 4 && !$scope.rgcomplete3.diapers_eco_text){
        toaster.pop('warning', '', 'Debes especificar la marca de pañal ecológico');
      }
      else if($scope.rgcomplete3.diapers == 5 && !$scope.rgcomplete3.diapers_text){
        toaster.pop('warning', '', 'Debes especificar tu marca de pañales favorita');
      }
      else if($scope.stepThreeForm.formula_registry.$invalid){
        toaster.pop('warning', '', 'Debes seleccionar una leche de fórmula');
      }
      else if($scope.rgcomplete3.milk == 12 && !$scope.rgcomplete3.milk_text){
        toaster.pop('warning', '', 'Debes especificar que leche de fórmula usas');
      }
      else{
        return true;
      }
      return false;
    };
  }])
  .controller('RegistryBasicStep4Ctrl', ['$scope',  'SessionService', 'toaster', '$state', 'webcrianza', 'webcomercio', 'ParentsResource',
  function ($scope, SessionService, toaster, $state, webcrianza, webcomercio, ParentsResource) {
    console.log(SessionService.getDataComplete());
    var data_step_four = SessionService.getDataComplete();
    $scope.rgcomplete4 = (data_step_four) ? data_step_four : {};
    $scope.rgcomplete4.webcrianza = {};
    $scope.rgcomplete4.webcomercio = {};

    $scope.webcrianza = webcrianza.results;
    $scope.webcomercio = webcomercio.results;

    $scope.continue4 = function(){
      if($scope.validateFormFour()){
        var session_data = SessionService.getDataComplete();
        console.log(Object.keys($scope.rgcomplete4.webcrianza));
        var websites = Object.keys($scope.rgcomplete4.webcrianza);
        ParentsResource.registryComplete(session_data, function(res){
          var complete_website = {
            complete: res.id,
            websites_ids: websites
          }
          ParentsResource.registryCompleteWebSites(complete_website, function(resInner){
            toaster.pop('success', '', resInner.detail);
            $state.go('main.registry-basic-info-complete');
          });
        });
      }
    };

    $scope.validateFormFour = function(){
      if(Object.keys($scope.rgcomplete4.webcrianza).length < 3){
        toaster.pop('warning', '', 'Debes seleccionar al menos 3 páginas web de Salud/Crianza');
      }
      else if(Object.keys($scope.rgcomplete4.webcomercio).length < 2){
        toaster.pop('warning', '', 'Debes seleccionar al menos 2 páginas web de Intercambio o Compra/Venta');
      }
      else{
        return true;
      }
      return false;
    }
  }]);