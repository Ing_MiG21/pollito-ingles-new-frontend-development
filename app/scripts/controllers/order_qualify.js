'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:OrderQualifyCtrl
 * @description
 * # OrderQualifyCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('OrderQualifyCtrl', ['$scope', '$state', 'order', 'ParentsResource', 'toaster', function ($scope, $state, order, ParentsResource, toaster){
  	$scope.order=order;

  	$scope.qualify= function(){
	  	var data = {
	    	"seller_or_buyer":1,
	        "user_scored":order.seller_id,
	        "score":$scope.qualification,
	        "order":order.id
	  	}
	  	ParentsResource.reputation(data, function(res){
	    	toaster.pop('success', '', 'Gracias por calificar');
	    	$state.go("main.profile.tabs-orders.orders");
	    });
  	}
}]);
