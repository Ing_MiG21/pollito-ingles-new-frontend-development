'use strict';
 
/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:AdvancedsearchresultsctrlCtrl
 * @description
 * # AdvancedsearchresultsctrlCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('Advancedsearchresultsctrl', ['$scope', '$filter', '$location', '$anchorScroll', '$state', 'toaster', 'AdvancedSearchResource', 'ShareInfoService' ,function ($scope, $filter, $location , $anchorScroll, $state, toaster, AdvancedSearchResource, ShareInfoService) {

  		$scope.resultSet = ShareInfoService.getProperty();
  		$location.hash('advanced_search_results');
  		$anchorScroll();


  }]);
