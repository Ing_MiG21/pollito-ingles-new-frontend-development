'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:FollowingCtrl
 * @description
 * # FollowingCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('FollowingCtrl', ['$scope','$rootScope', 'following', '$http', 'ParentsResource', 'toaster',
  function ($scope, $rootScope, following, $http, ParentsResource, toaster) {

      $scope.following = following.results;
      $scope.nextLink = following.next;
      $scope.loading = false;
      $scope.buttonText = 'Siguiendo';

      $scope.unfollow = function(index){
          ParentsResource.unfollow({id_user: $scope.following[index].user}, function(res){
              toaster.pop('success', '', res.detail);
              $scope.unfollwed = true;
              $rootScope.following = false;
              $scope.following.splice(index, 1);
          });
      }

      //This function fetches the users being followed
      $scope.more = function(){
          if($scope.nextLink){
              $scope.loading = true;
              $http.get($scope.nextLink).then(function(res){
                  for (var i = 0; i < res.data.results.length; i++) {
                      $scope.following.push(res.data.results[i]);
                  }
                  $scope.nextLink = res.data.next;
                  $scope.loading = false;
              });
          }
      }

  }]);
