'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:FaqCtrl
 * @description
 * # FaqCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('FaqCtrl', ['$scope','FaqResource', function ($scope,FaqResource) {

      $scope.hide = true;

      $scope.hideToggle = function(){
          $scope.hide = !$scope.hide;
      };
  }]);