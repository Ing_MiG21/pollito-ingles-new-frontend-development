'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:retrieve_passwordCtrl
 * @description
 * # retrieve_passwordCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('retrievePasswordCtrl', ['$scope', 'UserResource', 'toaster', function ($scope, UserResource, toaster) {

  	$scope.send = function () {
  		UserResource.retrievePassword($scope.user, function () {
             toaster.pop('success', '', 'Correo enviado');
        });
  	};

}]);
