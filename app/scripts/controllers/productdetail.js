'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ProductdetailCtrl
 * @description
 * # ProductdetailCtrl
 * Controller of the pollitoinglesApp
 */

angular.module('pollitoinglesApp')
  .controller('ProductdetailCtrl', ['$rootScope', '$scope','UserResource', 'publication', 'following', '$state', 'SessionService', '$stateParams', '$location', '$anchorScroll','$http','API','CommentResource','ContenttypeResource', 'ProductsResource', 'toaster','banners', 'BannersMetricsResource', 'metricas', '$window', 'ParentsResource', 
  function ($rootScope, $scope, UserResource, publication, following, $state, SessionService, $stateParams, $location, $anchorScroll, $http, API, CommentResource, ContenttypeResource, ProductsResource, toaster, banners, BannersMetricsResource, metricas, $window, ParentsResource) {

    $scope.publication = publication;
    $state.current.data.title = $scope.publication.product.name;
    $state.current.data.description = $scope.publication.product.description;
    $scope.url = $location.protocol() + '://' + $location.host() + '/p/' + publication.id + '/';
    $scope.hideMe = false;
    $scope.selected = 0;
    $anchorScroll('here');
    $scope.activeSession=false;
    $scope.buttonEdit = false;
    $scope.buttonExc = false;
    $scope.buyExc = false;
    $scope.showInappropriate = false;
    $scope.banners = banners.results;
    $scope.metricas = metricas.results;
    $scope.metricas_actual = [];
    $scope.filtrados = [];
    $scope.turnBanPrin = {};

    $scope.showAge = function(){
      var ageArr = $scope.publication.product.range_year_old.split(" ");
      if (ageArr.length > 1){
        var firstAge = ageArr[0];
        var lastAge = ageArr[ageArr.length-1];
        return firstAge + ' a ' + lastAge;
      }else{
        return ageArr[0];
      }
    }

    ParentsResource.getReputation({id_user:publication.user.pk}).$promise.then(function(reputation) {
      if(reputation[0].average_reputation){
          $scope.rating = reputation[0].average_reputation;
      }else {
          $scope.rating = 0;
          //falserating = 5 so that it renders 5 grey flowers in the view
          $scope.falserating = 5;
      }
    });

    ProductsResource.getMorePublications({uid:publication.user.pk}).$promise.then(function(res){
      $scope.relatedPublicatios = res.results;
    });

    angular.element(document).ready(function () {
      
      $scope.filtrados = $scope.banners.filter(filtroDetailProduct);
      
      if($scope.metricas.length == 0){
          //crear metricas de filtrados 
          for (var i = 1; i < $scope.filtrados.length; i++) {
            BannersMetricsResource.post({
                metrica: 0,
                prints: 0,
                clicks: 0,
                publicity: $scope.filtrados[i].pk
              },function(){
            });
          }

          BannersMetricsResource.post({
                metrica: 0,
                prints: 1,
                clicks: 0,
                publicity: $scope.filtrados[0].pk
              },function(){
          });

          angular.copy($scope.filtrados[0],$scope.turnBanPrin);


      }else{
        //Almenos no esta vacia asi que verifico que esten las metricas
        //de filtrados
        for (var j = 0; j < $scope.filtrados.length ; j++) {
         if(haveMetric($scope.filtrados[j],$scope.metricas) == false){
          //se crea su metrica
          BannersMetricsResource.post({
              metrica: 0,
              prints: 0,
              clicks: 0,
              publicity: $scope.filtrados[j].pk
              },function(){
            });
          }
        }

        //ahora de nuevo hago un get de metricas para trabajar en base a
        //data actualizada

    BannersMetricsResource.get(function(metric){

       
      angular.copy(metric,$scope.metricas_actual);
      
        //TODO ordenar las metricas por criterios de impresion de menor a mayor
        // y ver si ya la metrica existe para la actualizacion o hacer el post
        // de la nueva metrica

        $scope.metricas_actual.results.sort(function(cmp1,cmp2){
          if(cmp1.prints > cmp2.prints){
            return 1;
          }
        });

        var i;
        for (i = 0; i < $scope.filtrados.length; i++) {
          if($scope.filtrados[i].pk == $scope.metricas_actual.results[0].publicity){
            angular.copy($scope.filtrados[i],$scope.turnBanPrin);
            break;
          }
        }
    
        //TODO una vez se tenga el banner que va a ser impreso
        // se debe hacer post de la publicidad que se imprimio
        

        var mymetric = {
          metrica: 0,
          prints: ($scope.metricas_actual.results[0].prints + 1),
          clicks: $scope.metricas_actual.results[0].clicks,
          publicity: $scope.metricas_actual.results[0].publicity
        };

        BannersMetricsResource.update({"id": $scope.metricas_actual.results[0].id},mymetric,function(){
        });

      });


      }
 
    });

  

    function filtroDetailProduct(ban){
      if(ban.status == 1 && ban.url != "" && ban.banner == 2 && ban.location == 3){
        return true;
      }
    }

    function haveMetric(banfiltrado,metricas){
      for (var i = 0; i < metricas.length; i++){
        if(banfiltrado.pk == metricas[i].publicity){
          return true;
        }
      }
     return false; 
    }

    $scope.clicBanDetail = function(turn){
      if(turn.url){
        BannersMetricsResource.get(function(metric){
          var i,id;
          for (i = 0; i < metric.results.length; i++) {
            if(metric.results[i].publicity === turn.pk){
              id = metric.results[i].id;
              break;
            }
          }

          var mymetric = {
            metrica: 0,
            prints: metric.results[i].prints,
            clicks: (metric.results[i].clicks +1),
            publicity: metric.results[i].publicity
          };
          
          BannersMetricsResource.update({"id": id},mymetric,function(){
              $window.location.href = turn.url;
          });

        });
      }
    }

    if(!SessionService.hasToken()){

      if(SessionService.getId() != publication.user.pk) $scope.showInappropriate=true;

      $scope.activeSession=true;
      UserResource.getUser({id_user: SessionService.getId()},function(userData){
        $scope.me=userData;
      });

      CommentResource.get({"id": $stateParams.id},function(questions){
      $scope.comments=questions;
      });
    }

    $scope.reportAbuse = function(){
      if($scope.product_inapropiate){
        var radio = $scope.product_inapropiate;
      }else if($scope.product_adult){
        var radio = $scope.product_adult;
      }else{
        toaster.pop('warning', '', 'Debes seleccionar el tipo ');
      }
      var data = {
        "product": $scope.publication.product.id,
        "product_report": radio,
        "report": $scope.text_report
      }
      ProductsResource.addInapropiate(data,function(res) {
        if(res){
          toaster.pop('success', '', 'Producto Reportado');
        }else{
          toaster.pop('error', '', 'Error al reportar producto');
        }
      });

    }

    $scope.buy = function(){
      if(!SessionService.hasToken()){
        $state.go('main.checkout', {prd_id:$stateParams.id, sell_id:publication.user.pk});
      }else{
        toaster.pop('warning', '', 'Debes iniciar sesión para comprar');
      }
    }

    $scope.buttons = function(pub){
        var id = SessionService.getId();
        if(id == pub.user.pk){
          $scope.buttonEdit = true;
          $scope.buttonExc = false;
          return false;
        }else{

          switch(pub.status){
            case 1:
              $scope.buttonEdit = false;
              $scope.buttonExc = false;
              return true;

            case 2:
              $scope.buttonEdit = false;
              $scope.buttonExc = true;
              return false;

            case 3:
              $scope.buttonEdit = false;
              $scope.buttonExc = false;
              $scope.buyExc = true;
              return false; 
          }
        }
    }

    $scope.edit = function(){
        $state.go('main.productupdate', {id:$stateParams.id});
    }

    $scope.exchange = function(){
        $state.go('main.exchange', {prd_id:$stateParams.id, sell_id:publication.user.pk});
    }

    $scope.hide = function(){
        $scope.hideMe = !$scope.hideMe;
    }

    $scope.scroll = function(){
        $location.hash('scrollHere');
        $anchorScroll();
    }

    $scope.search = function(){
        $state.go('main.searchcategory', {category: $scope.publication.product.category.description});
    }

    $scope.clickSocialShared = function(social, pubId){
      /*$http.get(API.contentType)
      .then(
        function(succes)
          { 
          $scope.contentType = succes.data.content_type;
          
          var req = {
            method: 'POST',
            url: API.socialShared,
            data:{ 
              publication: pubId,
              social_type: social
            }
          }
          $http(req).then(function(succes){ 
          },function(error){});
        },
        function(error){}
      );*/
    }



    $scope.addcomment = function(){
      ContenttypeResource.get(function(questions){
          $scope.contentType = questions.content_type;

          if(!$scope.newComment) toaster.pop('error', '', 'Debes ingresar algún comentario ');
          else{
             var myComment = {
              comment:       $scope.newComment,
              object_pk:     $stateParams.id,
              user:          SessionService.getId(),
              user_name:     SessionService.getName(),
              user_email:    SessionService.getEmail(),
              content_type:  $scope.contentType,
              is_public:     1,
              site:          1
            }
            CommentResource.post(myComment,function(questions){
              $scope.newComment="";
              toaster.pop('success', '', 'Comentario agregado');
              CommentResource.get({"id": $stateParams.id},function(questions){
                $scope.comments=questions;
              });
            });
          }
      });
    };
}]);
