'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:MyproductsCtrl
 * @description
 * # MyproductsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('MyproductsCtrl', ['$scope', 'me', 'publications', '$http', 'ProductsResource', '$uibModal',
  function ($scope, me, publications, $http, ProductsResource, $uibModal) {

    $scope.me = me;
    $scope.publications=publications.results;
    $scope.nextPublications = publications.next;
        
    $scope.next = function(){
      if($scope.nextPublications){
          $http.get($scope.nextPublications).then(function(res){
            for (var i = 0; i < res.data.results.length; i++) {
              $scope.publications.push(res.data.results[i]);
            }
          $scope.nextPublications  = res.data.next;
        })
      }
    }

    $scope.delete = function(index){
      $scope.index = index;

      var modalInstance = $uibModal.open({
          templateUrl: 'views/delete-pub-modal.html',
          controller: 'ModalCtrl'
      });

      modalInstance.result.then(function () {
          $scope.deletePublication();
      }, function (){
          console.log('Modal dismissed at: ' + new Date());
      });
    }

    $scope.deletePublication = function(){
        ProductsResource.deletePublication({id:$scope.publications[$scope.index].id}, function(res){
            $scope.publications.splice($scope.index,1);
        });
    }
}]);
