'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ProfiletabsCtrl
 * @description
 * # ProfiletabsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ProfiletabsCtrl', ['$state', '$scope', '$rootScope',
  	function ($state, $scope, $rootScope) {

		$scope.isActive = function (state) {
			return $state.$current.name === 'main.profile.tabs.' + state;
		};
}]);
