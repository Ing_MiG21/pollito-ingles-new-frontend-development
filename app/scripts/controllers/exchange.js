'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ExchangeCtrl
 * @description
 * # ExchangeCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ExchangeCtrl', ['$scope',function ($scope){

  	$scope.myExchange=
  	{
		'amount':'20',
		'years':
		[
			{'year':'2014'},
			{'year':'2015'},
			{'year':'2016'}
		],
		'exchanges':
		[	
			{	'id':1,
				'title':'Zapatos Unixes RS21-Usados',
				'number':'002-989894568459-294555',
				'date':'01/04/2016',
				'nameBuyer':'Juana Chepepino',
				'correoBuyer':'chepepino@gmail.com',
				'status':'Completado',
				'total':'72.000,00'
			},
			{	'id':2,
				'title':'Zapatos Unixes RS21-Usados',
				'number':'002-989894568459-294555',
				'date':'01/04/2016',
				'nameBuyer':'Juana Chepepino',
				'correoBuyer':'chepepino@gmail.com',
				'status':'Completado',
				'total':'72.000,00'
			},
			{	'id':3,
				'title':'Zapatos Unixes RS21-Usados',
				'number':'002-989894568459-294555',
				'date':'01/04/2016',
				'nameBuyer':'Juana Chepepino',
				'correoBuyer':'chepepino@gmail.com',
				'status':'Completado',
				'total':'72.000,00'
			}
		]
	}
}]);
