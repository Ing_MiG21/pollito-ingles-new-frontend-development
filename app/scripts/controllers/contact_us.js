'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller: ContactUsCtrl
 * @description
 * # ContactUsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ContactUsCtrl', ['$scope', 'toaster', 'PollitoResource', 'rrss',
  function ($scope, toaster, PollitoResource, rrss) {
    $scope.contact = {};
    $scope.links_rrss = {
      facebook_link: '#',
      twitter_link: '#',
      pinterest_link: '#',
      instagram_link: '#',
      youtube_link: '#',
    };

    for (var i = 0; i < rrss.results.length; i++) {
      if($scope.links_rrss[rrss.results[i]['key']]){
        $scope.links_rrss[rrss.results[i]['key']] = rrss.results[i]['value'];
      }
    };

    $scope.sendContact = function(){
      if($scope.contactForm.$valid){
        PollitoResource.contactUs($scope.contact, function(res){
          $scope.contact.email='';
          $scope.contact.subject='';
          $scope.contact.description='';
          $scope.contactForm.$setPristine();
          toaster.pop('success', '', res.response.message);
        });    
      }
    }
 }]);