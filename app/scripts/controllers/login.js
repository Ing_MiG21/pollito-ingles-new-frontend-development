'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:loginCtrl
 * @description
 * # loginCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('loginCtrl',['$rootScope', '$scope', '$websocket', 'UserResource', 'AuthService', 'toaster', '$state', 'SessionService', 'NotificationsResource', 'ParentsResource',
  function ($rootScope, $scope, $websocket, UserResource, AuthService, toaster, $state, SessionService, NotificationsResource, ParentsResource) {
      
      $scope.fb_user = {};
      $scope.is_fb_login = false;
      $scope.userIsConnected = false;

      $scope.submitForm = function(){
          if ($scope.userForm.$valid) {
              $scope.login();
          }
      }

      $scope.intentFBLogin = function() {

          FB.getLoginStatus(function(response) {
              if (response.status == 'connected') {
                  $scope.logout();
                  $scope.loginFacebook();
              }else if (response.status === 'not_authorized') {
                  $scope.loginFacebook();
              }else {
              }
          });

          if(!$scope.userIsConnected) {
              $scope.loginFacebook();
          }
      };

      $scope.loginFacebook = function() {
          FB.login(function(response) {
              if (response.status == 'connected') {
                  $scope.userIsConnected = true;
                  $scope.info();
              }
          }, {scope: 'public_profile, email'});
      };

      $scope.info = function() {
          FB.api('/me', {fields: 'id,name,email'}, function(response) {
              $scope.$apply(function() {
                  $scope.fb_user.username = response.email;
                  $scope.fb_user.password = response.id;
                  $scope.is_fb_login = true;
              });
              $scope.login();
          });
      };

      $scope.registerFacebook = function(){
          $scope.fb_user.email = $scope.fb_user.username;
          $scope.fb_user.id_facebook = $scope.fb_user.password;
          $scope.fb_user.terms_conditions = 1;
          UserResource.create($scope.fb_user, function(res){
              $scope.login();
          });
      }

      $scope.login = function(){
          var data_login = ($scope.is_fb_login) ? $scope.fb_user : $scope.user;
          AuthService.login(data_login).then(function(res){

              toaster.pop('success', '', 'Bienvenido a Pollito Ingl\u00E9s');

              var email = SessionService.getEmail();
              UserResource.getUserByEmail({email:email},function(res){
                var me = res;
                SessionService.setId(me.pk);
                SessionService.setName(me.parent.first_name);
                SessionService.setLastname(me.parent.last_name);
                SessionService.setPhone(me.parent.phone_number);
                SessionService.setAddress(me.parent.direction);
                var name = SessionService.getName() + ' ' + SessionService.getLastname();
                $rootScope.UserName = name == ' '? SessionService.getEmail():name;
                $rootScope.incentives=true;

                ParentsResource.getRewards(function(res){
                  $rootScope.totalEggs = 0;
                  angular.forEach(res.results, function(value, key) {
                      $rootScope.totalEggs += parseInt(value.reward_point);
                  });
                });

                var notifications = NotificationsResource.getFrom({is_read:1},function(res){return res});
                notifications.$promise.then(function(res){$rootScope.notifications = res});

                $state.go('main.home');
              });
              
          }).catch(function(rejection) {
              if('data' in rejection){
                if('non_field_errors' in rejection.data){
                  if(!$scope.is_fb_login)
                    toaster.pop('error', '', rejection.data.non_field_errors[0]);
                }
              }
              if($scope.is_fb_login){
                $scope.registerFacebook();
              }
          });
      }

      $scope.logout = function() {
          FB.getLoginStatus(function(response) {
              if (response.status == 'connected') {
                  FB.logout(function() {
                      $scope.$apply(function() {
                          $scope.user = {};
                          $scope.userIsConnected = false;
                      });
                  });
              }
          });
      }

      $scope.sendActivationEmail = function() {
        if($scope.userForm.email.$valid){
          $scope.user.email = $scope.user.username;
          UserResource.reactivate($scope.user, function(res){
              toaster.pop('success', '', 'El correo de activación ha sido enviado');
              $state.go('main.login');
          });
        }else{
          toaster.pop('error', '', 'Debes indicar tu email');
        }
      }

  }]);
