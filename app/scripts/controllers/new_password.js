'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:retrieve_passwordCtrl
 * @description
 * # retrieve_passwordCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('newPasswordCtrl', ['$stateParams', '$scope', 'UserResource', 'toaster', '$state', function ($stateParams, $scope, UserResource, toaster, $state) {

  	$scope.send = function () {
  		UserResource.newPassword($stateParams, $scope.user,
            function(){
                toaster.pop('success', '', 'Contrase\u00F1a cambiada exitosamente');
                $state.go('main.login');
            }, function(error){
                toaster.pop('error', '', 'Error cambiando la contrase\u00F1a');
            });
  	};

}]);
