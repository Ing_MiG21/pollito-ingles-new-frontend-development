'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ExchangeCtrl
 * @description
 * # ExchangeCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ExchangeCtrl', ['$scope', 'publication', 'me', 'seller', 'OrderResource', 'toaster', '$state', 'SessionService', '$stateParams', '$location',
  function ($scope, publication, me, seller, OrderResource, toaster, $state, SessionService, $stateParams, $location) {

      $scope.publication = publication;
      $scope.seller = seller;
      $scope.isSubmited = false;
      $scope.showConfirm = false;
      $scope.exchange = { product: $scope.publication.id };
      $scope.textAreaDataUser = true;
      $scope.seller_name = ($scope.publication.user.parent.first_name != '') ? $scope.publication.user.parent.first_name + ' ' + $scope.publication.user.parent.last_name : $scope.publication.user.email;

      var myData = 
        "Email: " + me.email + "\n" +
        "Numero Telefonico: " + me.parent.phone_number + "\n" +
        "Direccion: " + me.parent.direction;

      $scope.itemSelected = function(){
        if($scope.exchange.status){
          $scope.textAreaDataUser = false;
          $scope.exchange.data_user = myData;
        }else{
          $scope.textAreaDataUser = true;
          delete $scope.exchange.data_user;
        }
      }

      $scope.sendExchange = function(){
        if ($scope.exchangeForm.$valid){
          OrderResource.setExchange($scope.exchange, function(res){
            $scope.exchange.data_user='';
            $scope.exchange.status=false;
            $scope.exchange.description='';
            $scope.textAreaDataUser = true;
            $scope.exchangeForm.$setPristine();
            toaster.pop('success', '', 'Email enviado exitosamente, pronto te contactarán');
            $state.go('main.home');
          });
        }
      }
  }]);