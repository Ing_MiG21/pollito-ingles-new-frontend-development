'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:UserproductprofileCtrl
 * @description
 * # UserproductprofileCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('UserproductprofileCtrl', ['$scope', '$stateParams', 'user', 'publications',
  	function ($scope, $stateParams, user, publications) {

      $scope.user = user;
      $scope.publications = publications.results;

  }]);
