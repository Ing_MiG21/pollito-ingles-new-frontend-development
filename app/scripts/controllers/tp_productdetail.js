'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:tp_ProductdetailCtrl
 * @description
 * # tp_ProductdetailCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('tp_ProductdetailCtrl', ['$scope', '$rootScope','$state', 'toaster', 'featured', 'SessionService', 'publication', 'VariationResource', 'VariationFilteredResource', 
    function ($scope, $rootScope ,$state, toaster, featured, SessionService, publication, VariationResource, VariationFilteredResource){
    
    $scope.publication = publication;
    $state.current.data.title = $scope.publication.product.name;
    $state.current.data.description = $scope.publication.product.description;
    $scope.featured = featured.results;
    $rootScope.detailTP = true;
    
    $scope.cart = {
        publication: {},
        quantity: 0,
        product: {}
    };

    function quantity() {
        $scope.quantitys=[];
        for (var i = 1; i <= $scope.publication.product.quantity; i++) {
           $scope.quantitys.push(i);
        }   
    };
    quantity();
    
    $scope.oneSelection = function(){
    	if ($scope.firstSelection){ 

            if (publication.product.product_variation[1]){
                var variationFilter = publication.product.product_variation[1].variation.id;            
                VariationResource.get({ id_product:publication.product.id, id_variation:$scope.firstSelection, id_filter:variationFilter }, function(data) {
                    $scope.twoVariations= data.results;
                });
            }else {
                VariationFilteredResource.single({ id_product:publication.product.id, id_variation:$scope.firstSelection }, function(data) {
                    console.log(data);
                    $scope.publication.price= data.price;
                    $scope.publication.product.quantity= data.quantity;
                    quantity();
                    $scope.data=data;
                    $scope.cart.publication.id = publication.id;
                    $scope.cart.publication.price= data.price;
                    $scope.cart.product.id = data.id;
                });      
            }
    	}
	};

    $scope.twoSelection = function(){
        if ($scope.secondSelection){ 
            VariationFilteredResource.double({ id_product:publication.product.id, id_variation:$scope.firstSelection, id_variationTwo:$scope.secondSelection }, function(data) {
                console.log(data);
                $scope.publication.price= data.price;
                $scope.publication.product.quantity= data.quantity;
                quantity();
                $scope.cart.publication.id = publication.id;
                $scope.cart.publication.price= data.price;
                $scope.cart.product.id = data.id;
            });
        }
    };

    $scope.pay = function(){
        SessionService.setCart($scope.cart);
        if(!SessionService.hasToken()){
            $state.go('main.checkout', {prd_id:publication.id, sell_id:publication.user.pk});
        }else{
            toaster.pop('warning', '', 'Debes iniciar sesión para comprar');
        }
    };
}])