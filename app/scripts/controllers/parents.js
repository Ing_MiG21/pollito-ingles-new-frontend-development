'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ParentsCtrl
 * @description
 * # ParentsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ParentsCtrl', ['$rootScope', '$scope', 'toaster', '$stateParams', '$state', 'Upload', 'API', 'SessionService', 'ParentsResource', 'me', 'Regex',
  function ($rootScope, $scope, toaster, $stateParams, $state, Upload, API, SessionService, ParentsResource, me, Regex) {

      $scope.me = {};
      $scope.child = {};
      $scope.profile = me;
      $scope.original = {};
      $scope.originalRs = {};
      $scope.disabled = true;
      $scope.regex2 = Regex.names;
      $scope.regex = Regex.cellphone;
      $scope.mom_emp = false;

      if($scope.profile.plan.code == "000002"){
        $scope.mom_emp = true;
      }

      angular.copy($scope.profile.parent, $scope.original);
      angular.copy($scope.profile, $scope.originalRs);

      $scope.update = function() {
          
        if ($scope.profile.parent.private_mes_active == undefined){
          $scope.profile.parent.private_mes_active = false;
        }
        //   if ($scope.profile.parent.public_profile == undefined)
        //     $scope.profile.parent.public_profile = false;
      
        if($scope.profile.plan.code == "000002"){

          if ($scope.profile.facebook == ""){
            $scope.profile.facebook = null;
          }
          if($scope.profile.twitter == ""){
            $scope.profile.twitter = null;
          }  
          if($scope.profile.instagram == ""){
            $scope.profile.instagram = null;
          }
          if($scope.profile.pinterest == ""){
           $scope.profile.pinterest = null;
          }
          if($scope.profile.youtube == ""){
           $scope.profile.youtube = null;
          }

           var data = {
              "first_name": $scope.profile.parent.first_name,
              "last_name": $scope.profile.parent.last_name,
              "direction": $scope.profile.parent.direction,
              "phone_number": $scope.profile.parent.phone_number,
              "private_mes_active": $scope.profile.parent.private_mes_active,
              "facebook": $scope.profile.facebook,
              "twitter": $scope.profile.twitter,
              "instagram": $scope.profile.instagram,
              "pinterest": $scope.profile.pinterest,
              "youtube": $scope.profile.youtube
          };

        }else{
          
           var data = {
              "first_name": $scope.profile.parent.first_name,
              "last_name": $scope.profile.parent.last_name,
              "direction": $scope.profile.parent.direction,
              "phone_number": $scope.profile.parent.phone_number,
              "private_mes_active": $scope.profile.parent.private_mes_active
          };

        }

          ParentsResource.update(data, function(res){
              $scope.me.parent = res;
              SessionService.setName($scope.me.parent.first_name);
              SessionService.setLastname($scope.me.parent.last_name);
              $rootScope.UserName = $scope.me.parent.first_name + ' ' + $scope.me.parent.last_name;
              toaster.pop('success', '', 'Datos actualizados');
              angular.copy(res, $scope.original);
              $scope.disabled = true;
              $state.go("main.profile.tabs.products");
          });
      }

      $scope.$watch('profile.parent.picture', function(newValue, oldValue){
          if(newValue != oldValue){
              Upload.upload({
                  url: API.parents + 'update/',
                  method: 'PUT',
                  data: {
                      "picture": newValue,
                  }
              }).then(function (resp) {
                  console.log('Success ' + resp.config.data.picture.name + 'uploaded. Response: ' + resp.data);
                  console.log(resp);
              }, function (error) {
                  console.log('Error status: ' + error.status);
              }, function (evt) {
                  var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  console.log('progress: ' + progressPercentage + '% ' + evt.config.data.picture.name);
              });
          }
      });

      $scope.$watch('profile.parent.first_name', function(newValue){
          if(newValue != $scope.original.first_name){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.parent.last_name', function(newValue){
          if(newValue != $scope.original.last_name){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.parent.direction', function(newValue){
          if(newValue != $scope.original.direction){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.parent.phone_number', function(newValue){
          if(newValue != $scope.original.phone_number){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.parent.private_mes_active', function(newValue){
          if(newValue != $scope.original.private_mes_active){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.facebook', function(newValue){
          if(newValue != $scope.originalRs.facebook){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.twitter', function(newValue){
          if(newValue != $scope.originalRs.twitter){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.instagram', function(newValue){
          if(newValue != $scope.originalRs.instagram){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.pinterest', function(newValue){
          if(newValue != $scope.originalRs.pinterest){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

      $scope.$watch('profile.youtube', function(newValue){
          if(newValue != $scope.originalRs.youtube){
              $scope.disabled = false;
          }else {
              $scope.disabled = true;
          }
      });

  }
]);
