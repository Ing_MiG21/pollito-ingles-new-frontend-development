'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:NotificationstabsCtrl
 * @description
 * # NotificationstabsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('NotificationstabsCtrl', ['$state', '$scope', '$rootScope', 'direct_message', 
  	function ($state, $scope, $rootScope, direct_message) {

  		$rootScope.dm_count = direct_message.count < 100 ? direct_message.count:'99+';

  		$scope.showBubble = function(){
  			return parseInt($rootScope.dm_count) > 0;
  		}

		$scope.isActive = function (state) {
			return $state.$current.name === 'main.profile.tabs.' + state;
		};

		$scope.isActiveNotif = function (state) {
			return $state.$current.name === 'main.profile.not-mess.' + state;
		};

}]);
