'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ModalCtrl
 * @description
 * # ModalCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ModalCtrl', ['$scope',  '$uibModalInstance', '$state',
  function ($scope, $uibModalInstance, $state) {

    $scope.ok = function () {
      $uibModalInstance.close(true);
    };

	  $scope.not = function() {
			$uibModalInstance.dismiss(false);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss(false);
      $state.go('main.home');
    };

}]);
