'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:QtyproductsbycategoryCtrl
 * @description
 * # QtyproductsbycategoryCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('QtyproductsbycategoryCtrl', ['$scope','$state','toaster', 'QtyProductsByCategoryResource', 'categories', function ($scope, $state, toaster, QtyProductsByCategoryResource, categories) {

  	
  	$scope.categories_home = {'ALIMENTACIÓN': 0, 'ANDADERAS':0 ,'BAÑO':0,'COCHES':0, 
  	  	  	'CUNAS Y CORRALES':0,'JUGUETES':0, 'OTROS':0, 'PORTABEBÉS':0, 'ROPA':0, 'SEGURIDAD':0, 
  	  	  	'SILLAS AUTO':0, 'SILLAS DE COMER':0};

  	$scope.categories = categories.results;
  	$scope.cont = 0;
 
    for(var cat in $scope.categories) {
      for(var i in $scope.categories_home){
        //console.log(i + " : " + $scope.categories[0].category__description.toUpperCase() + " : " + $scope.categories_home[i]);
        if($scope.categories[cat].category__description.toUpperCase() == i){
          $scope.categories_home[i] = $scope.categories[cat].products_qty;
        }
      }
    }

  }]);
