'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:UsersprofileCtrl
 * @description
 * # UsersprofileCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('Usersprofile2Ctrl', ['ParentsResource', '$scope', 'toaster', 'user', 'reportTypes', 'ReportsResource', 'SessionService', 'UserResource',
    function (ParentsResource, $scope, toaster, user, reportTypes, ReportsResource, SessionService, UserResource) {

      $scope.user = user;
      $scope.buyerRating = 1;
      $scope.sellerRating = 1;
      $scope.following = false;
      $scope.reports = reportTypes.results;

      $scope.report = {
          "selected": "",
          "description": ""
      };

      $scope.invalidReport = function(){
          return $scope.report.description == ''
      }

      $scope.reportUser = function(){
          if(SessionService.hasToken()){
              toaster.pop('error', '', 'Debes iniciar sesión para poder reportar a un usuario');
              return
          }
          var data = {
              "user_reported": $scope.user.pk,
              "report_type": $scope.report.selected.id,
              "description": $scope.report.description
          };
          ReportsResource.reportUser(data, function(res){
              toaster.pop('success', '', 'Acabas de reportar al usuario ' + user.parent.first_name);
              $scope.report = {};
          },function(error){
              toaster.pop('error', '', 'Error reportando al usuario');
          })
      }

      $scope.follow = function(){

          ParentsResource.follow({id_user: user.pk}, function(res){
            //   toaster.pop('success', '', 'Ahora sigues a ' + user.parent.first_name);
              toaster.pop('success', '', res.detail);
              $scope.following = true;
          });
      }

      $scope.rateUser = function(buyerOrSeller, points){

          if(SessionService.hasToken()){
              toaster.pop('warning', '', 'Debes iniciar sesi\u00F3n para poder calificar a un usuario');
              $scope.buyerRating = 1;
              $scope.sellerRating = 1;
              return
          }

          UserResource.getUser({id_user:SessionService.getId()}, function(me){

              var data = {
                  "user_scored": user.pk,
                  "user_scoring": me.pk,
                  "score": points,
                  "seller_or_buyer": buyerOrSeller
              };

              var type = buyerOrSeller == 1? 'vendedor' : 'comprador';

              ParentsResource.reputation(data, function(res){
                  toaster.pop('success', '', 'Acabas de calificar a ' + user.parent.first_name + ' como ' + type);
              }, function(error){
                  console.log(error.data.detail);
                  toaster.pop('error', '', 'Error calificando al usuario');
              });
          });
      }

  }]);
