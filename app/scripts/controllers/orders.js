'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:OrdersCtrl
 * @description
 * # OrdersCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('OrdersCtrl', ['$scope', '$http', '$timeout', 'orders',function ($scope, $http, $timeout, orders){

  	$scope.showPrintable=false;
    $scope.myOrders=orders;

    angular.forEach(orders.results, function(order){
      angular.forEach(order.associated_reputation, function(reputation){
        if(reputation.seller_or_buyer == 1) order.reputation=true;
      });   
    });
  	
  	$scope.next = function(){
      if($scope.myOrders.next){
        	$http.get($scope.myOrders.next).then(function(res){
            for (var i = 0; i < res.data.results.length; i++) {
              $scope.myOrders.results.push(res.data.results[i]);
          	}
  	    	$scope.myOrders.next = res.data.next;
      	})
    	}
  	}

    $scope.printDiv = function(divName, number) {
      $scope.printData = orders.results[number];
      $timeout(callAtTimeout, 30);

      function callAtTimeout(){     
        var printContents = document.getElementById(divName).innerHTML;
        var popupWin = window.open('', '_blank', 'width=800,height=700');
        popupWin.document.open();
        popupWin.document.write(
          '<html>'+
          '<head>'+
          '<link rel="stylesheet" href="bower_components/ng-img-crop/compile/minified/ng-img-crop.css" />'+
          '<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" />'+
          '<link rel="stylesheet" href="bower_components/AngularJS-Toaster/toaster.css" />'+
          '<link rel="stylesheet" href="bower_components/angularjs-slider/dist/rzslider.css" />'+
          '<link rel="stylesheet" href="styles/main.css">'+
          '</head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
      }
    }

    $scope.downloadDiv = function(divName, number) {
      $scope.printData = orders.results[number];
      $scope.showPrintable=true;
      $timeout(callAtTimeout, 30);
      html2canvas(document.getElementById(divName), {
          onrendered: function (canvas) {
              var data = canvas.toDataURL();
              var docDefinition = {
                pageSize: 'A6',
                pageMargins: [ 10, 15],
                content: [{ 
                    image: data,
                    width: 280
                  }]
              };
              pdfMake.createPdf(docDefinition).download("compra_#" + $scope.printData.id + ".pdf");
          }
      });
      function callAtTimeout() { $scope.showPrintable=false; }
  }

}]);
