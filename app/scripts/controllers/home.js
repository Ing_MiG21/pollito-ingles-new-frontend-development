'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the pollitoinglesApp
 * And banners metrics
 *
 */
angular.module('pollitoinglesApp')
  .controller('HomeCtrl', ['$scope', '$rootScope', 'publications', '$http', 'toaster','wordPressUrl', 'testimonies', 'banners', 'metricas', 'BannersMetricsResource', '$window',
  function ($scope, $rootScope, publications, $http, toaster, wordPressUrl, testimonies, banners, metricas, BannersMetricsResource, $window) {

      $scope.publications = publications.results;
      $scope.banners = banners.results;
      $scope.testimonies = testimonies;
      $scope.metricas = metricas.results;
      $scope.filtrados = [];
      
      $scope.nextPubs = publications.next;
      $scope.nextbanners = banners.next;
      $scope.nextTest = testimonies.next;

      angular.element(document).ready(function(){
        
        $scope.filtrados = $scope.banners.filter(filtroHome);
        
        if($scope.metricas.length == 0){
          //crear metricas de filtrados 
          for (var i = 0; i < $scope.filtrados.length; i++) {
            BannersMetricsResource.post({
                metrica: 0,
                prints: 1,
                clicks: 0,
                publicity: $scope.filtrados[i].pk
              },function(){
            });
          }

        }else{

          for (var k = 0; k < $scope.filtrados.length ; k++) {
           if(haveMetric($scope.filtrados[k],$scope.metricas) == false){
            BannersMetricsResource.post({
                metrica: 0,
                prints: 1,
                clicks: 0,
                publicity: $scope.filtrados[k].pk
              },function(){});
             }
          }

          BannersMetricsResource.get(function(metric){
            for(var j = 0; j < $scope.filtrados.length; j++){  

                BannersMetricsResource.update({"id": metric.results[j].id},{
                    metrica: 0,
                    prints: (metric.results[j].prints + 1),
                    clicks: metric.results[j].clicks,
                    publicity: metric.results[j].publicity
                },function(){});
            } 
          });

        }
      });


      function filtroHome(ban){
        if(ban.status == 1 && ban.url != "" && ban.banner == 2 && ban.location == 1){
          return true;
        }
      }

      function haveMetric(banfiltrado,metricas){
        for (var i = 0; i < metricas.length; i++){
          if(banfiltrado.pk == metricas[i].publicity){
            return true;
          }
        }
       return false; 
      }

      /*insertion of testimonies in publications*/
      $scope.publications.splice(5, 0, $scope.testimonies[0]);

      /*insertion of advertisements in publications*/
      var iBanner = 1;
      function merge(){
        for (; iBanner < $scope.filtrados.length; iBanner++) {
          if($scope.publications[4*iBanner]) $scope.publications.splice(4*iBanner, 0, $scope.filtrados[iBanner-1]);
          else break;
        }
      }
      merge();

      $scope.load = function(){

        if($scope.nextTest) {

            $http.get($scope.nextTest).then(function(res) {
                for (var i = 0; i < res.data.results.length; i++) {
                    $scope.testimonies.push(res.data.results[i]);
                }
                $scope.nextTest = res.data.next;
            })            
        }

        if($scope.nextbanners){

            $http.get($scope.nextbanners).then(function(res) {
                for (var i = 0; i < res.data.results.length; i++) {
                    $scope.banners.push(res.data.results[i]);
                }
                $scope.nextbanners = res.data.next;
            })            
        }

        if($scope.nextPubs){
            $http.get($scope.nextPubs).then(function(res){
                for (var i = 0; i < res.data.results.length; i++) {
                  $scope.publications.push(res.data.results[i]);
                }
                $scope.nextPubs = res.data.next;
                merge();
            })
        }else {
            toaster.pop('warning', '', 'No hay m\u00E1s productos para mostrar');
        }
      }

      function filtroContProduct(ban){
        if(ban.status == 1 && ban.banner == 2 && ban.location == 1){
         return true;
        }
      }

      $scope.viewBan = function(publication){
        if(publication.status == 1 && publication.banner == 2 && publication.location == 1){ 
          return true;
        }else{
          return false;
        }     
      }

      $scope.clickBanContCar = function(pub){
        if(pub.url){
          BannersMetricsResource.get(function(metric){
            var i,id;
            for (i = 0; i < metric.results.length; i++) {
              if(metric.results[i].publicity == pub.pk){
                id = metric.results[i].id;
                break;
              }
            }

            var mymetric = {
              metrica: 0,
              prints: metric.results[i].prints,
              clicks: (metric.results[i].clicks +1),
              publicity: metric.results[i].publicity
            };
            

            BannersMetricsResource.update({"id": id},mymetric,function(){
                $window.location.href = pub.url;
            });

          });
        } 
      }


/*      $scope.randomNumber = 0;*/


/*      $scope.getrandomNumber = function() {
          //return 0.5 - Math.random();
          $scope.randomNumber = 0.2 - Math.random();
      }

      $scope.getrandomNumber();
      $scope.load = function(){


          $scope.getrandomNumber();
          if($scope.nextTest) {

              $http.get($scope.nextTest).then(function(res) {
                  for (var i = 0; i < res.data.results.length; i++) {
                      $scope.testimonies.push(res.data.results[i]);
                  }
                  $scope.nextTest = res.data.next;
              })            
          }

          if($scope.nextPubs){
              $http.get($scope.nextPubs).then(function(res){
                  for (var i = 0; i < res.data.results.length; i++) {
                      $scope.publications.push(res.data.results[i]);
                  }
                  $scope.nextPubs = res.data.next;
              })
          }else {
              toaster.pop('warning', '', 'No hay m\u00E1s productos para mostrar');
          }
      }*/

      var urlBlocPollitoPost=
          wordPressUrl
          +'wp-json/wp/v2/posts?'
          +'_query=[0:4].'
          +'{title:title.rendered,excerpt:excerpt.rendered,link:link,featured_media:featured_media}';

      $http.get(urlBlocPollitoPost)
          .then(function(res){
              $scope.posts=res.data;
              angular.forEach($scope.posts, function(post){

                  post.title=post.title.replace(/<[^>]*>?/g, '');
                  post.excerpt=post.excerpt
                  .replace(/<[^>]*>?/g, '')             //removes everything that has "<code>"
                  .replace(/&nbsp;/,' ')                //removes everything that has "space"
                  .replace(/&(amp|quot|lt|gt);/,'')     //removes everything that has "<code>"
                  .replace(/\[[^\]]*]/g, '');           //removes everything that has "[code]"

                  if (post.excerpt.length>157) {
                      post.excerpt=post.excerpt.slice(0, 157)+"...";                    
                  }

                  if (post.title.length>42) {
                      post.title=post.title.slice(0, 42)+"...";
                  }
      
                  if (post.featured_media) {
                      var urlBlocPollitoMedia=
                          wordPressUrl
                          +"wp-json/wp/v2/media/"
                          +post.featured_media
                          +"?_query={source_url:source_url}";

                      $http.get(urlBlocPollitoMedia)
                          .then(function(res){
                              $scope.media=res.data;
                              post.img=$scope.media.source_url;
                          }, function(res){
                              console.log("error al cargar WP bloc Pollito Media");
                      });
                  }else post.img="https://pbs.twimg.com/profile_images/638576915218071552/61RjQCX_.png";
              });

          },function(res){
              console.log("error al cargar WP bloc Pollito");
      });
  }]);
