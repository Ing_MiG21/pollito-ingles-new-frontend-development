'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:UsersprofiletabsCtrl
 * @description
 * # UsersprofiletabsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('UsersprofiletabsCtrl', ['$state', '$scope', function ($state, $scope) {

      $scope.isActive = function (state) {
          return $state.$current.name === 'main.users.profile.tabs.' + state;
      };

}]);
