'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:SuggestionsCtrl
 * @description
 * # SuggestionsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('SuggestionsCtrl', ['ParentsResource', '$scope', 'toaster', function (ParentsResource, $scope, toaster) {

      $scope.addSuggestion = function(){
          var data = {
              "content": $scope.suggestion
          };

          ParentsResource.addSuggestion(data, function(res){
              toaster.pop('success', '', 'Sugerencia enviada');
              $scope.suggestion = '';
          }, function(error){
              toaster.pop('error', '', 'Sugerencia no enviada');
          })
      };

  }]);
