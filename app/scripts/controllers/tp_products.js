'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:tp-ProductsCtrl
 * @description
 * # tp-ProductsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('tp_ProductsCtrl', ['$scope', '$http','$anchorScroll', 'publications', 'best_seller',
    function ($scope, $http, $anchorScroll, publications, best_seller){
  
    	$scope.publications=publications.results;
      $scope.nextPublications = publications.next;
      $scope.showBanner=false;
      $scope.best_seller = best_seller.results;

      if(publications.results.length>3) $scope.showBanner=true;
          
      $scope.next = function(){
        if($scope.nextPublications){
            $http.get($scope.nextPublications).then(function(res){
              for (var i = 0; i < res.data.results.length; i++) {
                $scope.publications.push(res.data.results[i]);
              }
            $scope.nextPublications  = res.data.next;
          })
        }
      }

      $scope.scroll = function(){
        $anchorScroll();
      }

      // Despliegue de otros productos 

      $scope.categories = [];

      $scope.publications.forEach(function(arrayElem){
        $scope.categories.push(
          arrayElem.category.description
        );
      });

      $scope.categories = $scope.categories.filter(function(item, pos, self) {
        return self.indexOf(item) === pos;
      });

  }]);