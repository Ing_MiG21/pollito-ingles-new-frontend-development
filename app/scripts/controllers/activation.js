'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ActivationCtrl
 * @description
 * # ActivationCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('activationCtrl',['UserResource', '$stateParams', '$scope', 
  	function (UserResource,$stateParams,$scope) {
  		$scope.activated = true;
  	}]);
