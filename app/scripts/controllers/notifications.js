'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:NotificationsCtrl
 * @description
 * # NotificationsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('NotificationsCtrl', ['$scope', '$q', 'notifications', '$rootScope', 'NotificationsResource', '$http',
  function ($scope, $q, notifications, $rootScope, NotificationsResource, $http) {

    $scope.nextNotifications = notifications.next;
    $scope.notifications = notifications.results;

    $scope.getId = function(data){
      if(data != null){
        var json = angular.fromJson(data);
        return json['id'];
      }else{
        return 0;
      }
    }

    $scope.getName = function(data){
      if(data != null){
        try {
          var json = angular.fromJson(data);
          return json['name'];
        } catch (e) {
          return '';
        }
      }else{
        return '';
      }
    }
    
    $scope.next = function(){
        if($scope.nextNotifications){
            $http.get($scope.nextNotifications).then(function(res){
                for (var i = 0; i < res.data.results.length; i++) {
                  $scope.notifications.push(res.data.results[i]);
                }
                $scope.setRead();
                $scope.nextNotifications  = res.data.next;
            });
        }
    }

    function updateNotification(notification){
        var d = $q.defer();
        var notif = {
            is_read: true,
            id: notification.id
        }

        NotificationsResource.update(notif).$promise.then(
            function (response) {
                d.resolve(response);
            }, 
            function (response) {
                d.reject(response);
            }, 
            function (evt) {}
        );

        return d.promise;
    }

    $scope.setRead = function() {
        var promises = [];

        angular.forEach($scope.notifications, function(notification){
            if (notification.notification_type!='UDM'){
                var prom = updateNotification(notification);
                promises.push(prom);
            }
        });

        $q.all(promises).then(function(data) {
            NotificationsResource.getFrom({is_read:1}, function(res){
                $rootScope.notifications.count = (res.count >= 100) ? '+99':res.count;
                $rootScope.notifications.total = res.count;
            });
        });
    }

    $scope.setRead();

    $scope.delete = function(index){
        var notification = {
            "id": $scope.notifications[index].id,
            "deleted": true
        };
        NotificationsResource.update(notification, function(res){
            $scope.notifications.splice(index,1);
        });
    }

  }]);
