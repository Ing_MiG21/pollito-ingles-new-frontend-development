'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:SearchcategoryCtrl
 * @description
 * # SearchcategoryCtrl
 * Controller of the pollitoinglesApp
 */
 angular.module('pollitoinglesApp')
   .controller('SearchcategoryCtrl', ['$scope', '$rootScope', 'results', 'toaster', '$stateParams','$location','$anchorScroll', '$state', 
    function ($scope, $rootScope, results, toaster ,$stateParams, $location, $anchorScroll, $state) {
   		
         $rootScope.detailTP = false;
         $scope.category_name = $stateParams.category;

   		if($stateParams.category == 'ALIMENTACION')
   			$scope.category_name = 'Alimentación';
   		else if($stateParams.category == 'BANO')
   			$scope.category_name = 'Baño';
   		else if($stateParams.category == 'CUNAS-Y-CORRALES')
   			$scope.category_name = 'Cunas y corrales';
   		else if($stateParams.category == 'PORTABEBES')
   			$scope.category_name = 'Portabebés';
   		else if($stateParams.category == 'SILLAS-AUTO')
   			$scope.category_name = 'Sillas Auto';
   		else if($stateParams.category == 'SILLAS-DE-COMER')
   			$scope.category_name = 'Sillas de comer';

         $state.current.data.title = 'Categoría ' + $scope.category_name;
   		
   		if(results.length <= 0) 
   		{
   			toaster.pop('warning', '', 'B\u00FAsqueda sin resultados');
   		}
   		else
   		{
	       	$scope.resultSet = results;
			   $location.hash('search_results');
            $anchorScroll();   			
   		}

   }]);
