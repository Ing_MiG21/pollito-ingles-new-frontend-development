'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:FaqCtrl
 * @description
 * # FaqCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('Faq2Ctrl', ['$scope', 'FaqResource', function ($scope, FaqResource) {

      FaqResource.getFAQ(function(questions){
          $scope.questions = questions.results;
        //   angular.forEach($scope.questions, function(question,key){question.imagenes.push({"url":"www.google.com/picture" + key},{"url":"www.google.com/anotherpicture" + key})})
      })

  }]);
