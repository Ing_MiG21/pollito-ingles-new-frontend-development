'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:CheckoutCtrl
 * @description
 * # CheckoutCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
	.controller('CheckoutCtrl', ['$sce','$scope', 'reputation', 'publication', 'product_option', 'seller', 'totals', 'estados', 'ZoomResource', 'OrderResource', 'toaster', '$state', 'SessionService', '$stateParams', '$location', '$anchorScroll', 'ShareInfoService',
	function ($sce, $scope, reputation, publication, product_option, seller, totals, estados, ZoomResource, OrderResource, toaster, $state, SessionService, $stateParams, $location, $anchorScroll, ShareInfoService) {

		$scope.publication = publication;
		var tp_cart = SessionService.getCart();
		$scope.product_options = product_option;
		$scope.order_options = [];

		if (tp_cart != null && 'publication' in tp_cart){
			var quantity = (tp_cart.quantity <= 0) ? 1 : tp_cart.quantity;
			if('price' in tp_cart.publication)
				$scope.publication.price = tp_cart.publication.price * tp_cart.quantity;
			else
				$scope.publication.price = $scope.publication.price * tp_cart.quantity;
		}

		if(product_option != null){
			angular.forEach(product_option.product_variation, function(pv){
				var option = {
					"name": pv.variation.description,
					"value": pv.product_variations[0].name,
					"product_variation_id": pv.id
				}
				$scope.order_options.push(option);
			});
		}

		$scope.seller = seller;
		$scope.reputation = reputation;
		$scope.totals = totals.totals;
		$scope.tdcResponse = {};
		$scope.tdcVoucher = '';
		$scope.isSubmited = false;
		$scope.displayLoader = true;
		$scope.displayContinue = false;
		$scope.showConfirm = false;
		$scope.url = $location.absUrl();
		$scope.years = [];
		$scope.Math = window.Math;
		$scope.total_discount = null;
		var ckd = angular.copy(totals.totals);
		$scope.zones = estados;
		$scope.cities = [];
		$scope.agencies = [];
		$scope.order_shipping = {};
		$scope.zoomShp = false;
		$scope.tdc = {};
		$scope.shipping_method = 'zoom';
		$scope.envio_zone = null;
		$scope.envio_city = null;
		$scope.envio_agency = null;

		if($scope.reputation[0].average_reputation){
		  $scope.rating = $scope.reputation[0].average_reputation;
		}else {
		  $scope.rating = 0;
		  //falserating = 5 so that it renders 5 grey flowers in the view
		  $scope.falserating = 5;
		}

		$scope.setupCities = function(zone){
		$scope.order_shipping.shipping_zone = $scope.envio_zone.nombre;
		ZoomResource.getCiudades({codestado:zone.codestado}, function(res){
		  $scope.cities = res;
		});
		}

		$scope.setupAgency = function(city){
		$scope.order_shipping.shipping_city = $scope.envio_city.nombre;
		ZoomResource.getSucursales({codciudad:city.codciudad}, function(res){
		  $scope.agencies = res;
		}); 
		}

		$scope.setupAddress = function(agency){
		$scope.order_shipping.shipping_address = $scope.envio_agency.nombre + ': '+ $scope.envio_agency.direccion;
		$scope.order_shipping.shipping_method = 'Zoom';
		$scope.order_shipping.shipping_phone = $scope.envio_agency.telefonos;
		$scope.order_shipping.shipping_firstname = SessionService.getName();
		$scope.order_shipping.shipping_lastname = SessionService.getLastname();
		}

		$scope.updateShipping = function(){
			if($scope.shipping_method == 'pollito'){
			  $scope.order_shipping.shipping_zone = 'N/A';
			  $scope.order_shipping.shipping_city = 'N/A';
			  $scope.order_shipping.shipping_address = 'A conveniencia';
			  $scope.order_shipping.shipping_method = 'TiendaPollito';
			  $scope.order_shipping.shipping_phone = 'N/A';
			  $scope.order_shipping.shipping_firstname = SessionService.getName();
			  $scope.order_shipping.shipping_lastname = SessionService.getLastname();
			  $scope.zoomShp = true;
			}else if($scope.shipping_method == 'zoom'){
			  if($scope.envio_zone != null){
				$scope.order_shipping.shipping_zone = $scope.envio_zone.nombre;
				$scope.order_shipping.shipping_city = $scope.envio_city.nombre;
				$scope.order_shipping.shipping_address = $scope.envio_agency.nombre + ': '+ $scope.envio_agency.direccion;
				$scope.order_shipping.shipping_method = 'Zoom';
				$scope.order_shipping.shipping_phone = $scope.envio_agency.telefonos;
				$scope.order_shipping.shipping_firstname = SessionService.getName();
				$scope.order_shipping.shipping_lastname = SessionService.getLastname();
			  }
			  $scope.zoomShp = false;
			}
		}

		$scope.updateShipping();

		for (var i = 0; i <= 10; i++) {
		  $scope.years.push({value:moment().add(i,'year').year(), text:moment().add(i,'year').year()});
		}

		$scope.months = [];
		$scope.months.push({value:'01', text:'Enero'});
		$scope.months.push({value:'02', text:'Febrero'});
		$scope.months.push({value:'03', text:'Marzo'});
		$scope.months.push({value:'04', text:'Abril'});
		$scope.months.push({value:'05', text:'Mayo'});
		$scope.months.push({value:'06', text:'Junio'});
		$scope.months.push({value:'07', text:'Julio'});
		$scope.months.push({value:'08', text:'Agosto'});
		$scope.months.push({value:'09', text:'Septiembre'});
		$scope.months.push({value:'10', text:'Octubre'});
		$scope.months.push({value:'11', text:'Noviembre'});
		$scope.months.push({value:'12', text:'Diciembre'});

		$scope.mine = function(pub){
		  var id = SessionService.getId();
		  if(id == pub.user.pk){
			  return true
		  }else if (id === null) {
			  return false
		  }
		}

		$scope.updateDiscount = function(d){
			var discount_amount = ckd.subtotal * (d.discount/100);
			$scope.totals.discount = discount_amount;
			$scope.totals.percent_discount = window.Math.floor(d.discount);
			$scope.totals.subtotaldiscount = (ckd.subtotal - discount_amount)
			$scope.totals.total = (ckd.subtotal - discount_amount) + ckd.feed_tdc;
			$scope.totals.discount_id = d.reward;
			console.log(ckd);
		}

		$scope.sendPayment = function(){
			var is_valid = false;
			if(publication.product.is_chick){
			  if($scope.shipping_method=='zoom'){
				if($scope.envio_zone!=null && $scope.envio_city!=null && $scope.envio_agency!=null){
				  is_valid = true;
				}else{
				  toaster.pop('error', '', 'Debe seleccionar una direccion de envío');
				  is_valid = false;
				}
			  }else if($scope.shipping_method=='pollito'){
				if($scope.order_shipping.shipping_method == 'TiendaPollito'){
				  is_valid = true;
				}else{
				  toaster.pop('error', '', 'Debe seleccionar un método de envío');
				  is_valid = false;
				}
			  }
			}else{
			  is_valid = true;
			}

			if ($scope.tdcForm.$valid && is_valid) {
			  $location.hash('scrollHere');
			  $anchorScroll();
			  $scope.tdc.total_amount = $scope.totals.total;
			  $scope.showConfirm = true;
			  $scope.isSubmited = true;
			  $scope.displayLoader = true;
			  console.log($scope.totals);

			  OrderResource.instapagoPost($scope.tdc, function(res){
				if(res.status == 400){
				  $scope.isSubmited = false;
				}else{
				  //lanzo la creacion de la orden
				  $scope.tdcResponse = res.response;
				  $scope.tdcVoucher = res.response.voucher;

				  var dataOrder = {
					"seller_id": seller.pk,
					"comment": "----",
					"seller_fullname": (seller.parent.first_name !== '' && seller.parent.last_name !== '') ? seller.parent.first_name + ' ' + seller.parent.last_name:seller.email,
					"seller_email":seller.email,
					"seller_telephone":seller.parent.phone_number,
					"user": SessionService.getId(),
					"email":SessionService.getEmail(),
					"first_name":SessionService.getName(),
					"last_name":SessionService.getLastname(),
					"telephone":(SessionService.getPhone()!=null ? SessionService.getPhone():'N/A'),
					"ip":"127.0.0.1",
					"total": $scope.totals.total,
					"subtotal": $scope.totals.subtotal,
					"order_status": 2,
					"discount_id": $scope.totals.discount_id,
					"order_payment":{
						"payment_firstname":SessionService.getName(),
						"payment_lastname":SessionService.getLastname(),
						"payment_ci":"N/A",
						"payment_phone":(SessionService.getPhone()!=null ? SessionService.getPhone():'N/A'),
						"payment_address": "----",
						"payment_country": "Venezuela",
						"payment_method": "TDC",
						"payment_code": "tdc",
						"payment_commission":totals.totals.feed_tdc,
						"payment_ref":res.response.reference,
						"payment_response_code":res.response.reference,
						"payment_approval":res.response.approval,
						"payment_sequence":res.response.sequence,
						"payment_date":res.response.datetime
					},
					"product":{
						"publication": publication.id,
						"name": publication.product.name,
						"quantity": (tp_cart != null && 'quantity' in tp_cart) ? tp_cart.quantity:1,
						"price": $scope.publication.price,
						"iva": 0,
						"total": (publication.product.is_chick && $scope.totals.discount ? $scope.totals.subtotaldiscount: $scope.publication.price),
						"reward": (publication.product.is_chick && $scope.totals.discount ? $scope.totals.discount:0)
					},
					"order_shipping":$scope.order_shipping,
					"order_options":$scope.order_options
				  }

				  OrderResource.setOrder(dataOrder, function(res){
					$scope.displayLoader = false;
					$scope.displayContinue = true;
					SessionService.deleteCart();
					toaster.pop('success', '', 'Operación realizada exitosamente');
				  });
				}
			  });
			}
		}

		$scope.printDiv = function(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var popupWin = window.open('', '_blank', 'width=800,height=700');
			popupWin.document.open();
			popupWin.document.write(
			  '<html>'+
			  '<head>'+
			  '<link rel="stylesheet" href="bower_components/ng-img-crop/compile/minified/ng-img-crop.css" />'+
			  '<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" />'+
			  '<link rel="stylesheet" href="bower_components/AngularJS-Toaster/toaster.css" />'+
			  '<link rel="stylesheet" href="bower_components/angularjs-slider/dist/rzslider.css" />'+
			  '<link rel="stylesheet" href="styles/main.css">'+
			  '</head><body onload="window.print()">' + printContents + '</body></html>');
			popupWin.document.close();
		}

		$scope.downloadDiv = function(divName) {
			html2canvas(document.getElementById(divName), {
				onrendered: function (canvas) {
					var data = canvas.toDataURL();
					var docDefinition = {
						content: [
						  { 
							image: data,
							width: 300
						  }
						]
					};
					pdfMake.createPdf(docDefinition).download("Vouche.pdf");
				}
			});
		}
	}])
	.filter('trusted', ['$sce', function($sce) {
		var div = document.createElement('div');
		return function(text) {
		  div.innerHTML = text;
		  return $sce.trustAsHtml(div.textContent);
		};
	}]);
