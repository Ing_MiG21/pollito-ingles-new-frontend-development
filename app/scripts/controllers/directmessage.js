'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:DirectMessageCtrl
 * @description
 * # DirectMessageCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('DirectMessageCtrl', ['$scope', '$q', 'talkers', '$rootScope', 'ParentsResource', 'SessionService', 'NotificationsResource', 'direct_message', '$http', 'toaster',
  function ($scope, $q, talkers, $rootScope, ParentsResource, SessionService, NotificationsResource, direct_message, $http, toaster) {

    $scope.nextTalks = talkers.next;
    $scope.talkers = talkers.results;
    $scope.me = SessionService.getId();

    $scope.next = function(){
      if($scope.nextTalks){
          $http.get($scope.nextTalks).then(function(res){
            for (var i = 0; i < res.data.results.length; i++) {
              $scope.talkers.push(res.data.results[i]);
            }
            $scope.nextTalks  = res.data.next;
          });
      }
    }

    $scope.delete_conversation = function(sender_id){
      ParentsResource.deleteConversation({sid:sender_id}, function(res){
        toaster.pop('success', '', res.detail);
        ParentsResource.getTalkers().$promise.then(function(res){
          $scope.nextTalks = res.next;
          $scope.talkers = res.results;
        });
      })
    }

    function updateNotification(notification){
        var d = $q.defer();
        var notif = {
            is_read: true,
            id: notification.id
        }

        NotificationsResource.update(notif).$promise.then(
            function (response) {
                d.resolve(response);
            }, 
            function (response) {
                d.reject(response);
            }, 
            function (evt) {}
        );

        return d.promise;
    }

    $scope.setRead = function() {
        var promises = [];

        angular.forEach(direct_message.results, function(notification){
            var prom = updateNotification(notification);
            promises.push(prom);
        });

        $q.all(promises).then(function(data) {
            NotificationsResource.getFrom({is_read:1}, function(res){
                $rootScope.notifications.count = (res.count >= 100) ? '+99':res.count;
                $rootScope.notifications.total = res.count;
            });
            NotificationsResource.getFrom({is_dm:1,is_read:1}, function(res){
                $rootScope.dm_count = res.count;
            });
        });
    }

    $scope.setRead();

  }]);
