'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:SaleQualifyCtrl
 * @description
 * # SaleQualifyCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('SaleQualifyCtrl', ['$scope', '$state', 'sale', 'ParentsResource', 'toaster', function ($scope, $state, sale, ParentsResource, toaster){
  	$scope.sale=sale;

  	$scope.qualify= function(){
  		var data = {
	    	"seller_or_buyer":2,
	        "user_scored":sale.user,
	        "score":$scope.qualification,
	        "order":sale.id
	  	}
	  	ParentsResource.reputation(data, function(res){
	    	toaster.pop('success', '', 'Gracias por calificar');
	    	$state.go("main.profile.tabs-orders.sales");
	    });
  	}
}]);
