'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:FooterCtrl
 * @description
 * # FooterCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('FooterCtrl', ['$rootScope' ,'$scope', '$state', 'rrss', '$location', '$anchorScroll', 
    function ($rootScope, $scope, $state, rrss, $location, $anchorScroll) {
      
    $scope.links_rrss = {
      facebook_link: '#',
      twitter_link: '#',
      pinterest_link: '#',
      instagram_link: '#'
    };

    for (var i = 0; i < rrss.results.length; i++) {
      if($scope.links_rrss[rrss.results[i]['key']]){
        $scope.links_rrss[rrss.results[i]['key']] = rrss.results[i]['value'];
      }
    };

    $scope.gotoBuy = function(){
      $location.hash('buyBtn');
      $anchorScroll();
      $rootScope.hide = false;
    }

    $scope.search = function(str){  
        if($scope.query == null || $scope.query == '') 
        {
            toaster.pop('error', '', 'Debe indicar un criterio de b\u00FAsqueda');
            return false;    
        }
        var data = { query: $scope.query };
        data['is_chick'] = ($rootScope.isTPollito) ? 1:0;
        $state.go('main.generalsearch', data);
    }
  }]);