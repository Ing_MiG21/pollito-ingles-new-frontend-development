'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ProductCtrl
 * @description
 * # ProductCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ProductCtrl', ['$scope','$q', 'publication', 'categories', 'ProductsResource', 'toaster', '$state','Upload', 'API', 'Regex', 'regions', 'Cities',
  function ($scope, $q, publication, categories, ProductsResource, toaster, $state, Upload, API, Regex, regions, Cities) {

    $scope.regex = Regex.price;
    $scope.publication = angular.copy(publication);
    $scope.product = angular.copy(publication.product);
    $scope.categories = categories.results;
    $scope.regions = regions.results;
    $scope.cities = [];

    $scope.product.region = _.find(regions.results, function(region){
      return region.id == publication.product.city.region;
    });

    if($scope.product.region){
      Cities.clear();
      Cities.getCities(API.locations + 'city/', $scope.product.region.geoname_id).then(function(res){
        $scope.cities = res;
      });
    }
    
    $scope.regionChanged = function(){
      //Clear data in Cities factory to refresh on new search
      Cities.clear();
      if($scope.product.region){
        Cities.getCities(API.locations + 'city/', $scope.product.region.geoname_id).then(function(res){
          if(res.length== 0) toaster.pop('warning', '', 'Seleccione un estado diferente');
          $scope.cities = res;
          $scope.product.city='';
        });
      }
    }

    $scope.pictures = [];

    /*IMAGE CROP*/
    $scope.myImage = '';
    $scope.myCropp = '';
   
    angular.element(document).ready(function(){
      $scope.testing = function(elem){
        $scope.myImage = elem;
        return true;
      };
    });
     /*IMAGE CROP*/


    function uploadPictures(picture, index){
      var d = $q.defer();
      Upload.upload({

        url: $scope.product.product_image[index] ? 
          API.products + 'images/' + $scope.product.product_image[index].id + '/':
          API.products + 'images/',
        method: $scope.product.product_image[index] ? 'PUT':'POST',

        data: {
          "picture": picture,
          "description": picture.name,
          "product": publication.product.id
        }
      }).then(function (response) {
        d.resolve(response);
      }, function (response) {
        d.reject(response);
      }, function (evt) {});

     return d.promise;
    }

    /*function addPicture(picture, pid) {
        var d = $q.defer();
        Upload.upload({
          url: API.products + 'images/',
          data: {
              "picture": picture.picture,
              "description": picture.picture.name,
              "product": pid
          }
        }).then(function (response) {
          d.resolve(response);
        }, function (response) {
          d.reject(response);
        }, function (evt) {});

        return d.promise;
    }*/

    $scope.edit = function(){

      $scope.product.city=$scope.product.city.id;
      $scope.product.category=$scope.product.category.id;
      $scope.publication.privilege = (publication.user.is_premium == true)?2:1;
      $scope.publication.enable_comments= (publication.enable_comments=='Si')?true:false;
      $scope.publication.category=$scope.product.category;
      $scope.publication.product = publication.product.id;
      $scope.publication.user= publication.user.pk;
      
      ProductsResource.update({id:publication.product.id}, $scope.product, function(res){

        var promises =  [  
          $scope.pictures[0] ? uploadPictures($scope.pictures[0],0):null,
          $scope.pictures[1] ? uploadPictures($scope.pictures[1],1):null,
          $scope.pictures[2] ? uploadPictures($scope.pictures[2],2):null
        ];

        $q.all(promises).then(function(data) {
          ProductsResource.updatePublication({id:publication.id}, $scope.publication, function(res){
            toaster.pop('success', '', 'Publicaci\u00F3n actualizada');
            $state.go("main.productdetail", {id:publication.id});
          })
        });
      });
    }
}]);