'use strict';

/**
 * @ngdoc function
 * @name pollitoinglesApp.controller:ProductsCtrl
 * @description
 * # ProductsCtrl
 * Controller of the pollitoinglesApp
 */
angular.module('pollitoinglesApp')
  .controller('ProductsCtrl', ['$scope', '$q', 'blockUI', 'ProductsResource', 'categories', 'toaster', 'SessionService', '$state', 'Upload', 'API', 'UserResource', '$http', 'me', 'Regex', 'regions', 'Cities', '$uibModal', 'can_public', '$location', '$anchorScroll',
    function ($scope, $q, blockUI, ProductsResource, categories, toaster, SessionService, $state, Upload, API, UserResource, $http, me, Regex, regions, Cities, $uibModal, can_public, $location, $anchorScroll) {
        //blockUI.start();
      $scope.product = {};
      $scope.nextCategories = categories.next;
      $scope.categories = categories.results
      $scope.regex = Regex.price;
      $scope.published = false;
      $scope.croppedPic = [];
      $scope.pictures = [];
      $scope.cities = [];
      $scope.me = me;
      $scope.total_images=0;
      
      $location.hash('vender_intercambiar');
      $anchorScroll();

      /*LOCATION LOGIC*/

      $scope.selected = {
          country: {},
          region: {},
          city: {}
      };

      $scope.regions = regions.results;

      $scope.loading = false;

      //Modal limit publications
      if(can_public.success==false){
        var modalInstance = $uibModal.open({
            templateUrl: 'views/sell-modal.html',
            controller: 'ModalCtrl',
            size: 'lg',
            openedClass: 'modal-open-sell',
            keyboard: false,
            backdrop: 'static'
        });
      }

      $scope.regionChanged = function(){
          //Clear data in Cities factory to refresh on new search
          $scope.cities = [];
          Cities.clear();
          if($scope.product.region){
              Cities.getCities(API.locations + 'city/', $scope.product.region.geoname_id).then(function(res){
                  $scope.cities = res;
              });
          }
      }

      /*LOCATION LOGIC END*/

      if (categories.next) {
        $http.get($scope.nextCategories).then(function(res){
          for (var i = 0; i < res.data.results.length; i++) {
              $scope.categories.push(res.data.results[i]);
          }
        });
      }

    $scope.itemSelected = function(item){
        
    }

    $scope.product.age = [];

    // toggle selection for a given fruit by name
    $scope.toggleSelection = function toggleSelection(age) {
        var idx = $scope.product.age.indexOf(age);
        // is currently selected
        if (idx > -1) {
            $scope.product.age.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.product.age.push(age);
        }
    };

    $scope.addProduct = function(){

        if($scope.checkValidity()){

            $scope.published = true;

            $scope.product.category = _.find($scope.categories, function(category){
                return category.description.toUpperCase() == $scope.selectedcategory;
            });

            var data = {
                "user": $scope.me.pk,
                "category": $scope.product.category.id,
                "name": $scope.product.name,
                "description": $scope.product.description,
                "condition": $scope.product.condition,
                "range_year_old": $scope.product.age.join(" "),
                "gender": $scope.product.gender,
                "price": $scope.product.price,
                "city": $scope.product.city.id
            };

            ProductsResource.save(data, function(product){
                var data2 = {
                    "product": product.id,
                    "category": product.category,
                    "user": $scope.me.pk,
                    "privilege": $scope.me.is_premium?2:1,
                    "status": $scope.product.status,
                    "price": $scope.product.price,
                    //TODO add enable comments in view?
                    "enable_comments": $scope.product.comments || true
                }

                if ($scope.pictures) {
                    var promises =  [  
                      $scope.pictures[0] ? uploadPicture($scope.pictures[0], product.id):null,
                      $scope.pictures[1] ? uploadPicture($scope.pictures[1], product.id):null,
                      $scope.pictures[2] ? uploadPicture($scope.pictures[2], product.id):null
                    ];
                    
                    $q.all(promises).then(function(data) {
                        ProductsResource.addPublication(data2, function(res){
                            toaster.pop('success', '', 'Nuevo producto publicado');
                            $state.go('main.home');
                        });
                    });
                }
            });
        }
    }

    function uploadPicture(picture, pid) {
        var d = $q.defer();
        Upload.upload({
          url: API.products + 'images/',
          data: {
              "picture": picture.picture,
              "description": picture.picture.name,
              "product": pid
          }
        }).then(function (response) {
          d.resolve(response);
        }, function (response) {
          d.reject(response);
        }, function (evt) {});

        return d.promise;
    }

    $scope.checkValidity = function(){

        if($scope.productForm.what_do.$invalid){
            toaster.pop('warning', '', 'Debes seleccionar Vender, Vender/Intercambiar o Intercambiar');
        }else if($scope.productForm.use_condition.$invalid){
            toaster.pop('warning', '', 'Debes indicar la condición del producto');
        }else if($scope.productForm.category.$invalid){
            toaster.pop('warning', '', 'Debes seleccionar una categoría');
        }else if ($scope.productForm.nombreProducto.$invalid) {
            toaster.pop('warning', '', 'Debes agregar el nombre del producto');
        }else if ($scope.productForm.precioProducto.$invalid) {
            toaster.pop('warning', '', 'Debes agregar el precio del producto');
        }else if ($scope.productForm.pic1.$invalid) {
            toaster.pop('warning', '', 'Debes agregar la imagen del producto');
        }else if ($scope.productForm.gender.$invalid) {
            toaster.pop('warning', '', 'Debes indicar el género');
        }else if ($scope.product.age === undefined || $scope.product.age.length < 1) {
            toaster.pop('warning', '', 'Debes indicar la edad');
        }else if ($scope.productForm.city.$invalid) {
            toaster.pop('warning', '', 'Debes indicar una ciudad');
        }else if($scope.productForm.$valid){
          return true;
        }

    }

    $scope.ModalMembership = function(){

    }
      

    /*
    Labels and products logic, discarted

    $scope.products = products;
    $scope.labels = labels.results;
    $scope.labelsSelected = [];
    $scope.addLabel = function(label){
    var index = $scope.labelsSelected.indexOf(label);
    index == -1?$scope.labelsSelected.push(label):$scope.labelsSelected.splice(index, 1);
    }

    */

  }]);
