'use strict';

angular
.module('pollitoinglesApp')
.directive('compareTo', function () {
	return {
		require: "ngModel",
		scope: {
			theValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {

			ngModel.$validators.compareTo = function(modelValue) {				
				return modelValue == scope.theValue;
			};

			scope.$watch("theValue", function() {
				ngModel.$validate();
			});
		}
	}
})