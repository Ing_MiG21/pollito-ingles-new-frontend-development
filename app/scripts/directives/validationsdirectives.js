'use strict';

angular
.module('pollitoinglesApp')
.directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseInt(digits,10);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
})
.directive('phoneFormat', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return digits;
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
})
.directive('onlyAlpha', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var alpha = val.replace(/[^a-zA-ZñÑ ]/g, '');

            if (alpha !== val) {
              ctrl.$setViewValue(alpha);
              ctrl.$render();
            }
            return alpha;
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
})