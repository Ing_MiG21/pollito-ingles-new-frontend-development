'use strict';

/**
 * @ngdoc directive
 * @name pollitoinglesApp.directive:whenScrolled
 * @description
 * # whenScrolled
 */
angular.module('pollitoinglesApp')
  .directive('whenScrolled', function ($window, $document) {
      return{
          restrict: 'A',
          link: function(scope, elem, attrs){
              //Get a list of elements of size 1 and need the first element
              var element = elem[0];
              //Load more elements when scrolled past a limit
              angular.element($window).bind("scroll", function(){
                if($window.pageYOffset + $window.innerHeight == $document.height()) {
                  if(scope.nextLink != undefined){
                      //We can give any function which loads more elements into the list
                      scope.$apply(attrs.whenScrolled);
                  }
                }
              });
          }
      }
  });
