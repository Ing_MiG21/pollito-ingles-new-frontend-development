angular.module('pollitoinglesApp')
.config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/");

    $stateProvider
    .state('root',{
        abstract: true,
        template: '<div ui-view/>'

    })
    /* ESTADOS TIENDAPOLLITO*/
    .state('main.tienda-pollito',{        
        url: '/tienda-pollito',        
        views: {
            '': {
                'templateUrl': 'views/tienda-pollito.home.html'
            },

            'tienda-pollito-categories@main.tienda-pollito': {
                'templateUrl': 'views/tienda-pollito.categories.html',
                controller: 'QtyproductsbycategoryCtrl',
                resolve: {
                    categories: function(QtyProductsByCategoryResource) {
                        return QtyProductsByCategoryResource.getQtyProductsByCategory().$promise;
                    }
                }                
            },
            'products@main.tienda-pollito': {
                'templateUrl': 'views/tienda-pollito.products.html',
                controller: 'tp_ProductsCtrl',
            },
            'others_products@main.tienda-pollito': {
                'templateUrl': 'views/tienda-pollito.others_products.html',
                controller: 'tp_ProductsCtrl'
            },
            'tienda-pollito-recommended@main.tienda-pollito': {
                'templateUrl': 'views/tienda-pollito.recommended.html',
                controller: 'TpRecommendedCtrl'
            },
            'tienda-pollito-collection@main.tienda-pollito': {
                'templateUrl':'views/tienda-pollito.collection.html'
            }
        },
        resolve: {
            publications: function(ProductsResource){
                return ProductsResource.getPublicationsTiendaPollito().$promise;
            },
            featured: function(ProductsResource){
                return ProductsResource.getFeaturedTiendaPollito().$promise;
            },
            best_seller: function(ProductsResource){
                return ProductsResource.getBestSellerTiendaPollito().$promise;
            }
        },
        data:{
            title: 'Tienda Pollito',
        }
    })       
    .state('main.tienda-pollito.carousel',{
        url: '/main', 
        views: {
            carousel: {
                templateUrl: 'views/tienda-pollito.carousel.html',
                controller: 'tp_CarouselCtrl'
            }
        }
    })
    .state('main.tienda-pollito.products-detail',{
        url: '/product-detail/:id',
        views: {
            detail: {
                templateUrl: 'views/tienda-pollito.products-detail.html',
                controller: 'tp_ProductdetailCtrl',
                resolve: {
                    publication: function(ProductsResource, $stateParams){
                        return ProductsResource.getPublication({id:$stateParams.id}).$promise;
                    }
                } 
            }
        },
        data:{
            title: '',
            description: ''
        }
    })
    .state('main.tienda-pollito.category-results',{
        url: '/categoria/:category',
        views: {
            products: {
                templateUrl: 'views/tienda-pollito.category-results.html',
                controller: 'SearchcategoryCtrl',
                resolve: {
                    results: function(SearchResource, $stateParams){
                        return SearchResource.searchCategory({is_chick:1, item:$stateParams.category}).$promise;
                    }
                }
            }
        },
        data:{
            title: 'Categorías',
        }
    })
    /* ESTADOS PAGINA POLLITO INGLES.COM.VE*/ 
    .state('main', {
        parent: 'root',        
        views: {
            '':{
                templateUrl: 'views/root.index.html',
            },
            'session-bar@main': {
              templateUrl: 'views/main.sesion-bar.html',
            },
            'header@main': {
                templateUrl: 'views/main.header.html',
                controller: 'MainCtrl'
            },
            'search-by-age@main': {
                templateUrl: 'views/main.search_old.html',
                controller: 'SearchAgeCtrl'
            },
            'advanced_search@main': { 
                templateUrl: 'views/main.advanced_search.html',
                controller: 'AdvancedsearchCtrl as asctrl',
                resolve:{
                    regions: function (RegionsResource, defaultCountry) {
                        return RegionsResource.getRegions({cid:defaultCountry}).$promise;
                    }
                }
            },
            'footer@main': {
              templateUrl: 'views/main.footer.html',
              controller: 'FooterCtrl',
              resolve: {
                rrss: function(PollitoResource){
                    return PollitoResource.confPi().$promise;
                }
              }
            }
        }
    })
    .state('main.home', {        
        url: '/',        
        views: {
            '': { 
                templateUrl: 'views/main.html',
                controller: 'HomeCtrl',
            },
            'categories@main.home': { 
                templateUrl: 'views/main.categories.html'
            },            
            'products@main.home': { 
                templateUrl: 'views/main.products.html'
            }

        },
        resolve: {
            publications: function(ProductsResource){
                return ProductsResource.getPublications().$promise;
            },
            testimonies: function(TestimoniesResource){
                return TestimoniesResource.getRandomTestimonies().$promise;
            },
            banners: function(BannersResource){
                return BannersResource.getBanners().$promise;
            },
            metricas: function(BannersMetricsResource){
                return BannersMetricsResource.get().$promise;
            },
            regions: function (RegionsResource, defaultCountry) {
                return RegionsResource.getRegions({cid:defaultCountry}).$promise;
            }
        },
        data:{
            title: 'Bienvenido a Pollito Inglés',
        }
    })
    .state('main.advanced-search-results',{
        url: '/busqueda-avanzada',
        templateUrl: 'views/main.advanced_search_results.html',
        controller: 'Advancedsearchresultsctrl',
        data:{
            title: 'Resultados de la búsqueda avanzada',
        }
    })    
    .state('main.login', {
        url: '/login',
        templateUrl: 'views/main.login.html',
        controller: 'loginCtrl',
        data:{
            title: 'Inicio de sesión',
        }
    })
    .state('main.register',{
        url: '/registrate',
        templateUrl: 'views/main.register.html',
        controller: 'registerCtrl',
        data:{
            title: 'Regístrate',
        }
    })
    .state('main.incentivos',{
        url: '/incentivos',
        templateUrl: 'views/main.incentivos.html',
        controller: 'IncentivosCtrl',
        resolve: {
            rewards: function(ParentsResource, SessionService){
                return (!SessionService.hasToken()) ? ParentsResource.getRewards().$promise : null;
            },
            incentivos: function(MembershipsResource){
                return MembershipsResource.getDiscounts().$promise;
            }
        },
        data:{
            title: 'Incentivos',
        }
    })
    .state('main.order',{
        url: '/orden',
        templateUrl: 'views/profile_order.html',
        controller: 'ProfilePrintCtrl',
        data:{
            title: 'Ordenes',
        }
    })
    .state('main.exitosa',{
        url: '/exitosa',
        templateUrl: 'views/compra_exitosa.html',
        controller: 'CompraExitosaCtrl',
        data:{
            title: 'Compra exitosa',
        }
    })
    .state('main.activation',{
        url: '/activation/:token',
        templateUrl: 'views/main.activation.html',
        controller: 'activationCtrl',
        data:{
            title: 'Activa tu cuenta',
        }
    })
    .state('main.categories',{
        url: '/categoria',
        templateUrl: 'views/main.categories_old.html',
        data:{
            title: 'Categorías',
        }
    })
    .state('main.complete-your-registry',{
        url:'/completa-tu-registro',
        templateUrl: 'views/main.start-complete-registry.html',
        data:{
            title: 'Completa tu registro',
        }
    })
    .state('main.registry-basic-info',{
        url:'/informacion-basica-paso-1',
        templateUrl: 'views/main.registry-basic-info.html',
        controller: 'RegistryBasicStep1Ctrl',
        resolve: {
            income: function(ParentsResource){
                return ParentsResource.income().$promise;
            },
            check: function(ParentsResource, toaster, $state, $q){
                var deferred = $q.defer();
                ParentsResource.checkRegistry(function(resp){
                    if(resp.success){
                        $state.go('main.home');
                        toaster.pop('warning', '', resp.details);
                        deferred.reject();
                    }else{
                        deferred.resolve(resp);
                    }
                });
                return deferred.promise;
            }
        },
        data:{
            title: 'Completa tu registro - Paso 1',
        }
    })
    .state('main.registry-basic-info-2',{
        url:'/informacion-basica-paso-2',
        templateUrl: 'views/main.registry-basic-info-2.html',
        controller: 'RegistryBasicStep2Ctrl',
        resolve: {
            check: function(ParentsResource, toaster, $state, $q){
                var deferred = $q.defer();
                ParentsResource.checkRegistry(function(resp){
                    if(resp.success){
                        $state.go('main.home');
                        toaster.pop('warning', '', resp.details);
                        deferred.reject();
                    }else{
                        deferred.resolve(resp);
                    }
                });
                return deferred.promise;
            },
            data_complete: function(SessionService, toaster, $state){
                if(SessionService.getDataComplete()==null){
                    $state.go('main.login');
                    toaster.pop('warning', '', 'Debes iniciar sesión');
                }else{
                    return true;
                }
            }
        },
        data:{
            title: 'Completa tu registro - Paso 2',
        }
    })
    .state('main.registry-basic-info-3',{
        url:'/informacion-basica-paso-3',
        templateUrl: 'views/main.registry-basic-info-3.html',
        controller: 'RegistryBasicStep3Ctrl',
        resolve: {
            check: function(ParentsResource, toaster, $state, $q){
                var deferred = $q.defer();
                ParentsResource.checkRegistry(function(resp){
                    if(resp.success){
                        $state.go('main.home');
                        toaster.pop('warning', '', resp.details);
                        deferred.reject();
                    }else{
                        deferred.resolve(resp);
                    }
                });
                return deferred.promise;
            },
            data_complete: function(SessionService, toaster, $state){
                if(SessionService.getDataComplete()==null){
                    $state.go('main.login');
                    toaster.pop('warning', '', 'Debes iniciar sesión');
                }else{
                    return true;
                }
            }
        },
        data:{
            title: 'Completa tu registro - Paso 3',
        }
    })  
    .state('main.registry-basic-info-4',{
        url:'/informacion-basica-paso-4',
        templateUrl: 'views/main.registry-basic-info-4.html',
        controller: 'RegistryBasicStep4Ctrl',
        resolve: {
            check: function(ParentsResource, toaster, $state, $q){
                var deferred = $q.defer();
                ParentsResource.checkRegistry(function(resp){
                    if(resp.success){
                        $state.go('main.home');
                        toaster.pop('warning', '', resp.details);
                        deferred.reject();
                    }else{
                        deferred.resolve(resp);
                    }
                });
                return deferred.promise;
            },
            webcrianza: function(ParentsResource){
                return ParentsResource.websites({ht:1}).$promise;
            },
            webcomercio: function(ParentsResource){
                return ParentsResource.websites({ht:2}).$promise;
            },
            data_complete: function(SessionService, toaster, $state){
                if(SessionService.getDataComplete()==null){
                    $state.go('main.login');
                    toaster.pop('warning', '', 'Debes iniciar sesión');
                }else{
                    return true;
                }
            }
        },
        data:{
            title: 'Completa tu registro - Paso 4',
        }
    })
    .state('main.registry-basic-info-complete',{
        url:'/informacion-basica-completa',
        templateUrl: 'views/main.registry-basic-info-complete.html',
        data:{
            title: 'Completaste tu registro exitosamente',
        }
    })
    .state('main.searchcategory', {
        url: '/categoria/:category',
        templateUrl: 'views/main.search-categories.html',
        controller: 'SearchcategoryCtrl',
        resolve: {
            results: function(SearchResource, $stateParams){
                return SearchResource.searchCategory({is_chick:0,item:$stateParams.category}).$promise;
            }
        },
        data:{
            title: 'Búsqueda por categoría',
        }
    })
    .state('main.generalsearch', {
        url: '/busqueda/:query/:is_chick/',
        views: {
            '': { 
                templateUrl: 'views/main.search.html',
                controller: 'SearchcategoryCtrl',
                resolve: {
                    results: function(SearchResource, $stateParams) {
                        return SearchResource.searchItem({is_chick:$stateParams.is_chick,item:$stateParams.query}).$promise;
                    }
                }                
            },
        },
        data:{
            title: 'Búsqueda general',
        }
    })
    .state('main.testimonies',{
        url: '/testimonios',
        templateUrl: 'views/main.testimonies.html',
        controller: 'TestimoniesCtrl',
        resolve: {
            testimonies: function(TestimoniesResource){
                return TestimoniesResource.get().$promise;
            }
        },
        data:{
            title: 'Testimonios',
        }
    })
    .state('main.suggestions',{
        url: '/sugerencias',
        templateUrl: 'views/suggestions.html',
        controller: 'SuggestionsCtrl',
        data:{
            title: 'Sugerencias',
        }
    })
    .state('main.retrieve_password',{
        url: '/retrieve_password',
        templateUrl: 'views/main.retrieve_password.html',
        controller: 'retrievePasswordCtrl',
        data:{
            title: 'Recuperar contraseña',
        }
    })
    .state('main.new_password',{
        url: '/new_password/:token',
        templateUrl: 'views/main.new_password.html',
        controller: 'newPasswordCtrl',
        data:{
            title: 'Nueva contraseña',
        }
    })
    .state('main.faq',{
        url: '/faq',
        templateUrl: 'views/faq.html',
        data:{
            title: 'Preguntas frecuentes',
        }
    })
    .state('main.contact-us',{
        url: '/contactanos',
        templateUrl: 'views/main.contact-us.html',
        controller: 'ContactUsCtrl',
        resolve: {
            rrss: function(PollitoResource){
                return PollitoResource.confPi().$promise;
            }
        },
        data:{
            title: 'Contáctanos',
        }
    })    
    .state('main.terms-conditions',{
        url: '/terminos-condiciones',
        templateUrl: 'views/main.terms-conditions.html',
        data:{
            title: 'Términos y condiciones',
        }
    })
    .state('main.copyright-policies',{
        url: '/copyright-policies',
        templateUrl: 'views/main.copyright-policies.html',
        data:{
            title: 'Políticas de derecho de autor',
        }
    })
    .state('main.privacy-policy',{
        url: '/privacy-policy',
        templateUrl: 'views/main.privacy-policy.html',
        data:{
            title: 'Políticas de privacidad',
        }
    })
    .state('main.acceptable-use-policy',{
        url: '/acceptable-use-policy',
        templateUrl: 'views/main.acceptable-use-policy.html',
        data:{
            title: 'Políticas de uso aceptable',
        }
    })
    .state('main.about-us',{
        url: '/sobre-nosotros',
        templateUrl: 'views/main.about-us.html',
        controller: 'nosotrosCtrl',
        resolve: {
            users: function(PollitoResource){
                return PollitoResource.team().$promise;
            }
        },
        data:{
            title: 'Sobre nosotros',
        }
    })
    .state('main.registry-plans',{
        url: '/planes-de-registro',
        templateUrl: 'views/main.plans.html',
        controller: 'PlansCtrl',
        resolve:{
            planes: function(MembershipsResource){
                return MembershipsResource.getPlans().$promise;
            }
        },
        data:{
            title: 'Planes de registro',
        }
    })
    .state('main.profile',{
        abstract: true,
        url: '/perfil',
        templateUrl: 'views/main.profile.html',
        controller: 'ProfileCtrl',
        resolve: {
            me: function(UserResource, SessionService){
                return UserResource.getUser({id_user: SessionService.getId()}).$promise;
            },
            reputation: function(ParentsResource, SessionService){
                return ParentsResource.getReputation({id_user: SessionService.getId()}).$promise;
            },
            check: function(ParentsResource){
                return ParentsResource.checkRegistry().$promise;
            }
        },
        data:{
            title: 'Mi perfil',
        }
    })
    .state('main.profile.tabs',{
        abstract: true,
        views: {
            tabs: {
                templateUrl: 'views/profile.tabs.html',
                controller: 'ProfiletabsCtrl',
                resolve: {
                    direct_message: function(NotificationsResource){
                        return NotificationsResource.get({is_dm:1,is_read:1}).$promise;
                    }
                }
            }
        }
    })
    .state('main.profile.tabs.products',{
        url: '/productos',
        views: {
            tab: {
                templateUrl: 'views/profile.tabs.products.html',
                controller: 'MyproductsCtrl',
                resolve: {
                    publications: function(ProductsResource, SessionService){
                        return ProductsResource.getPublicationsFrom({uid:SessionService.getId()}).$promise;
                    }
                }
            }
        },
        data:{
            title: 'Mis publicaciones',
        }
    })
    .state('main.profile.tabs.comments',{
        url: '/comentarios',
        views: {
            tab: {
                templateUrl: 'views/profile.tabs.comments.html',
            }
        },
        data:{
            title: 'Comentarios',
        }
    })
    .state('main.profile.tabs.followers',{
        url: '/seguidores',
        views: {
            tab: {
                templateUrl: 'views/profile.tabs.followers.html',
                controller: 'FollowersCtrl',
                resolve: {
                    followers: function(ParentsResource, SessionService){
                        return ParentsResource.followers({uid:SessionService.getId()}).$promise;
                    },
                    following: function(ParentsResource, SessionService){
                        return ParentsResource.following({uid:SessionService.getId()}).$promise;
                    }
                }
            }
        },
        data:{
            title: 'Seguidores',
        }
    })
    .state('main.profile.tabs.following',{
        url: '/siguiendo',
        views: {
            tab: {
                templateUrl: 'views/profile.tabs.following.html',
                controller: 'FollowingCtrl',
                resolve: {
                    following: function(ParentsResource, SessionService){
                        return ParentsResource.following({uid:SessionService.getId()}).$promise;
                    }
                }
            }
        },
        data:{
            title: 'Siguiendo',
        }
    })
    .state('main.profile.not-mess',{
        abstract: true,
        views: {
            notifications: {
                templateUrl: 'views/profile.tabs.not-mess.html',
                controller: 'NotificationstabsCtrl',
                resolve: {
                    direct_message: function(NotificationsResource){
                        return NotificationsResource.get({is_dm:1,is_read:1}).$promise;
                    }
                }
            }
        }
    })
    .state('main.profile.not-mess.notifications',{
        url: '/notificaciones',
        views: {
            tab: {
                templateUrl: 'views/profile.not-mess.notifications.html',
                controller: 'NotificationsCtrl',
                resolve: {
                    notifications: function(NotificationsResource){
                        return NotificationsResource.get().$promise;
                    }
                }
            }
        },
        data:{
            title: 'Notificaciones',
        }
    })
    .state('main.profile.not-mess.messages',{
        url: '/mensajes',
        views: {
            tab: {
                templateUrl: 'views/profile.not-mess.messages.html',
                controller: 'DirectMessageCtrl',
                resolve: {
                    talkers: function(ParentsResource){
                        return ParentsResource.getTalkers().$promise;
                    },
                    direct_message: function(NotificationsResource){
                        return NotificationsResource.get({is_dm:1,is_read:1}).$promise;
                    }
                }
            }
        },
        data:{
            title: 'Mensajes',
        }
    })
    .state('main.profile.talk',{
        url: '/chat/:user/',
        views: {
            'talk': {
                templateUrl: 'views/profile.not-mess.talk.html',
                controller: 'TalkMessageCtrl',
                resolve: {
                    messages: function(ParentsResource,$stateParams){
                        return ParentsResource.getMessages({uid:$stateParams.user}).$promise;
                    },
                    me: function(UserResource, SessionService){
                        return UserResource.getUser({id_user:SessionService.getId()}).$promise
                    }
                }
            }
        },
        data:{
            title: 'Conversación',
        }
    })
    .state('main.profile.tabs-orders',{
        views: {
            tabsorders: {
                templateUrl: 'views/profile.tabsorders.html',
                controller: 'ProfiletabOrdersCtrl' 
            }
        }
    })
    .state('main.profile.tabs-orders.orders',{
        url: '/ordenes',
        views: {
            tabsorder: {
                templateUrl: 'views/profile.tabsorders.orders.html',
                controller: 'OrdersCtrl',
                resolve: {
                    orders: function(OrderResource){
                        return OrderResource.getOrders().$promise;
                    }
                }  
            }
        },
        data:{
            title: 'Mis ordenes',
        }
    })
    .state('main.profile.tabs-orders.sales',{
        url: '/ventas',
        views: {
            tabsorder: {
                templateUrl: 'views/profile.tabsorders.sales.html',
                controller: 'SalesCtrl',
                resolve:{
                    sales: function(OrderResource){
                        return OrderResource.getSales().$promise;
                    } 
                }
            }
        },
        data:{
            title: 'Mis ventas',
        }
    })    
/*   
    view List Exchanges

     .state('main.profile.tabs-orders.exchanges',{
        url: '/intercambios',
        views: {
            tabsorder: {
                templateUrl: 'views/profile.tabsorders.exchange.html',
                controller: 'ExchangeCtrl'
            }
        }
    })*/
    .state('main.profile.order',{
        url: '/orden/:number',
        views: {
            order: {
                templateUrl: 'views/order.detail.html',
                controller: 'OrderDetailCtrl',
                resolve: {
                    order: function(OrderResource, $stateParams){
                        return OrderResource.orderDetail({id_order:$stateParams.number}).$promise;
                    } 
                } 
            }
        },
        data:{
            title: 'Detalles de orden',
        }
    })
    .state('main.order-qualify',{
        url: '/perfil/orden/calificar/:number',
        templateUrl: 'views/order.qualify.html',
        controller: 'OrderQualifyCtrl',
        resolve: {
            order: function(OrderResource, $stateParams){
                return OrderResource.orderDetail({id_order:$stateParams.number}).$promise;
            } 
        },
        data:{
            title: 'Califica al vendedor',
        }
    })
    .state('main.profile.sale',{
        url: '/venta/:number',
        views: {
            order: {
                templateUrl: 'views/sale.detail.html',
                controller: 'SaleDetailCtrl',
                resolve: {
                    sale: function(OrderResource, $stateParams){
                        return OrderResource.saleDetail({id_order:$stateParams.number}).$promise;
                    } 
                }   
            }
        },
        data:{
            title: 'Detalles de la orden de venta',
        }
    })
    .state('main.sale-qualify',{
        url: '/perfil/venta/calificar/:number',
        templateUrl: 'views/sale.qualify.html',
        controller: 'SaleQualifyCtrl',
        resolve: {
            sale: function(OrderResource, $stateParams){
                return OrderResource.saleDetail({id_order:$stateParams.number}).$promise;
            } 
        },
        data:{
            title: 'Califica al comprador',
        }
    })

/*  View Detail exchange

   .state('main.profile.exchange',{
        url: '/intercambio/:number',
        views: {
            order: {
                templateUrl: 'views/exchange.detail.html',
                controller: 'ExchangeDetailCtrl'
            }
        }
    })*/

    .state('main.profile.sendmessage',{
        url: '/sendmessage/:pk',
        views: {
            mymsj: {
                templateUrl: 'views/main.profile.sendmessage.html',
                controller: 'SendMessageCtrl',
                resolve: {
                    user: function(UserResource, $stateParams){
                        return UserResource.getUser({id_user:$stateParams.pk}).$promise;
                    }
              },  
            }
        },
        data:{
            title: 'Mensaje privado',
        }
    })
    .state('main.users',{
        abstract: true,
        url: '/usuario',
        template: '<div ui-view/>'
    })
    .state('main.users.profile',{
        url: '/:uid',
        templateUrl: 'views/main.profile.html',
        controller: 'UsersProfileCtrl',
        resolve: {
            user: function(UserResource, $stateParams){
                return UserResource.getUser({id_user:$stateParams.uid}).$promise;
            },                    
            session_following: function(ParentsResource, SessionService, $stateParams){
                if(!SessionService.hasToken())
                    return ParentsResource.following({uid:SessionService.getId()}).$promise;
                else
                    return null;
            },
            reputation: function(ParentsResource, $stateParams){
                return ParentsResource.getReputation({id_user: $stateParams.uid }).$promise;
            }
        },
        data: {
            profile: 'public',
            title: 'Perfil'
        }
    })
    .state('main.users.profile.tabs',{
        abstract: true,
        views: {
            tabs: {
                templateUrl: 'views/user.profile.tabs.html',
                controller: 'UsersprofiletabsCtrl'
            }
        }
    })
    .state('main.users.profile.tabs.products',{
        url: '/productos',
        views: {
            tab: {
                templateUrl: 'views/profile.tabs.products.html',
                controller: 'UserproductprofileCtrl',
                resolve: {
                    publications: function(ProductsResource, $stateParams){
                        return ProductsResource.getPublicationsFrom({uid:$stateParams.uid}).$promise;
                    }
                }
            }
        },
        data:{
            title: 'Publicaciones',
        }
    })
    .state('main.users.profile.tabs.comments',{
        url: '/comentarios',
        views: {
            tab: {
                templateUrl: 'views/profile.tabs.comments.html',
            }
        },
        data:{
            title: 'Comentarios',
        }
    })
    .state('main.users.profile.tabs.followers',{
        url: '/seguidores',
        views: {
            tab: {
                templateUrl: 'views/profile.tabs.followers.html',
                controller: 'FollowersCtrl',
                resolve: {
                    followers: function(ParentsResource, user){
                        return ParentsResource.followers({uid:user.pk}).$promise;
                    },
                    following: function(ParentsResource, user){
                        return ParentsResource.following({uid:user.pk}).$promise;
                    }
                }
            }
        },
        data:{
            title: 'Seguidores',
        }
    })
    .state('main.users.profile.tabs.following',{
        url: '/siguiendo',
        views: {
            tab: {
                templateUrl: 'views/profile.tabs.following.html',
                controller: 'FollowingCtrl',
                resolve: {
                    following: function(ParentsResource, user){
                        return ParentsResource.following({uid:user.pk}).$promise;
                    }
                }
            }
        },
        data:{
            title: 'Siguiendo',
        }
    })
    .state('main.profileupdate',{
        url: '/editar/perfil',
        templateUrl: 'views/profile.update.html',
        controller: 'ParentsCtrl',
        resolve: {
            me: function(UserResource, SessionService){
                return UserResource.getUser({id_user:SessionService.getId()}).$promise
            }
        },
        data:{
            title: 'Editar perfil',
        }
    })
    .state('main.children',{
        url: '/hijos',
        templateUrl: 'views/children.html',
        controller: 'ChildrenCtrl',
        resolve: {
            me: function(UserResource, SessionService){
                return UserResource.getUser({id_user:SessionService.getId()}).$promise
            }
        },
        data:{
            title: 'Hijos',
        }
    })
    .state('main.sell',{
        url: '/vender',
        templateUrl: 'views/main.sell.html',
        controller: 'ProductsCtrl',
        resolve: {
            categories: function(ProductsResource){
                return ProductsResource.getCategories().$promise;
            },
            me: function (UserResource, SessionService) {
                return UserResource.getUser({id_user: SessionService.getId()}).$promise;
            },
            regions: function (RegionsResource, defaultCountry) {
                return RegionsResource.getRegions({cid:defaultCountry}).$promise;
            },
            can_public: function(ParentsResource){
                return ParentsResource.checkPlanProducts().$promise;
            }
        },
        data:{
            title: 'Publica',
        }
    })
    .state('main.productupdate',{
        url: '/producto/editar/:id',
        templateUrl: 'views/product.update.html',
        controller: 'ProductCtrl',
        resolve: {
            publication: function(ProductsResource, $stateParams){
                return ProductsResource.getPublication({id:$stateParams.id}).$promise;
            },
            // labels: function(ProductsResource){
            //     return ProductsResource.getLabels().$promise;
            // },
            categories: function(ProductsResource){
                return ProductsResource.getCategories().$promise;
            },
            regions: function (RegionsResource, defaultCountry) {
                return RegionsResource.getRegions({cid:defaultCountry}).$promise;
            }
        },
        data:{
            title: 'Actualizar publicación',
        }
    })
    .state('main.productdetail',{
        url: '/producto/:id',
        templateUrl: 'views/product.detail.html',
        controller: 'ProductdetailCtrl',
        resolve: {
            publication: function(ProductsResource, $stateParams){
                return ProductsResource.getPublication({id:$stateParams.id}).$promise;
            },
            following: function(ParentsResource, SessionService){
                return (!SessionService.hasToken()) ? ParentsResource.following({uid:SessionService.getId()}).$promise : null;
            },
            banners: function(BannersResource){
                return BannersResource.getBanners().$promise;
            },
            metricas: function(BannersMetricsResource){
                return BannersMetricsResource.get().$promise;
            }
        },
        data:{
            title: '',
            description: ''
        }
    })
    .state('main.checkout',{
        url: '/checkout/:sell_id/:prd_id',
        templateUrl: 'views/main.checkout.html',
        controller: 'CheckoutCtrl',
        resolve: {
            publication: function($q, $state, toaster, ProductsResource, $stateParams){
                var deferred = $q.defer();
                ProductsResource.getPublication({id:$stateParams.prd_id}, function(resp){
                    if(resp.product.is_chick){
                        if(resp.product.quantity > 0){
                            deferred.resolve(resp);
                        }else{
                            $state.go('main.productdetail',{id:$stateParams.prd_id});
                            toaster.pop('warning', '', 'Producto fuera de stock');
                            deferred.reject();
                        }
                    }else{
                        if (resp.status == 4){
                            $state.go('main.productdetail',{id:$stateParams.prd_id});
                            toaster.pop('warning', '', 'Este producto ya ha sido vendido');
                            deferred.reject();
                        }else{
                            deferred.resolve(resp);
                        }
                    }
                });
                return deferred.promise;
            },
            product_option: function(ProductsResource, SessionService){
                var tp_cart = SessionService.getCart();
                console.log(tp_cart);
                return (tp_cart != null && 'product' in tp_cart && 'id' in tp_cart.product) ? ProductsResource.getProduct({id:tp_cart.product.id}).$promise : null;
            },
            seller: function(UserResource, $stateParams){
                return UserResource.getUser({id_user: $stateParams.sell_id}).$promise;
            },
            reputation: function(ParentsResource, $stateParams){
                return ParentsResource.getReputation({id_user: $stateParams.sell_id}).$promise;
            },
            totals: function(OrderResource, $stateParams, SessionService){
                var tp_cart = SessionService.getCart();
                var data = {pid: $stateParams.prd_id};
                if (tp_cart != null && 'product' in tp_cart){
                    data['poid'] = tp_cart.product.id;
                    data['qty'] = tp_cart.quantity;
                }
                return OrderResource.getTotals(data).$promise;
            },
            estados: function(ZoomResource){
                return ZoomResource.getEstados().$promise;
            }
        },
        data:{
            title: 'Checkout',
        }
    })
    .state('main.plan_checkout',{
        url: '/adquirir_plan/:p_id',
        templateUrl: 'views/main.plan_checkout.html',
        controller: 'PlanCheckoutCtrl',
        resolve: {
            me: function($q, $state, toaster, UserResource, SessionService, $stateParams){
                var deferred = $q.defer();
                UserResource.getUser({id_user: SessionService.getId()}, function(res){
                    if(res.plan.id == $stateParams.p_id){
                        //$state.go('main.home');
                        toaster.pop('warning', '', 'Ya estas usando este plan');
                        deferred.reject();
                    }else{
                        deferred.resolve(res);
                    }
                });
                return deferred.promise;
            },
            totals: function(MembershipsResource, $stateParams){
                return MembershipsResource.getPlanTotals({pid: $stateParams.p_id}).$promise;
            },
            plan: function(MembershipsResource, $stateParams){
                return MembershipsResource.getPlan({pid:$stateParams.p_id}).$promise;
            }
        },
        data:{
            title: 'Adquiere un plan',
        }
    })
    .state('main.exchange',{
        url: '/intercambiar/:sell_id/:prd_id',
        templateUrl: 'views/main.exchange.html',
        controller: 'ExchangeCtrl',
        resolve: {
            publication: function($q, $state, toaster, ProductsResource, $stateParams){
                var deferred = $q.defer();
                ProductsResource.getPublication({id:$stateParams.prd_id}, function(resp){
                    if(resp.product.is_chick){
                        if(resp.product.quantity > 0){
                            deferred.resolve(resp);
                        }else{
                            $state.go('main.productdetail',{id:$stateParams.prd_id});
                            toaster.pop('warning', '', 'Producto fuera de stock');
                            deferred.reject();
                        }
                    }else{
                        if (resp.status == 4){
                            $state.go('main.productdetail',{id:$stateParams.prd_id});
                            toaster.pop('warning', '', 'Este producto ya ha sido vendido');
                            deferred.reject();
                        }else{
                            deferred.resolve(resp);
                        }
                    }
                });
                return deferred.promise;
            },
            seller: function(UserResource, $stateParams){
                return UserResource.getUser({id_user: $stateParams.sell_id}).$promise;
            },
            me: function(UserResource, SessionService){
                return UserResource.getUser({id_user: SessionService.getId()}).$promise;
            }
        },
        data:{
            title: 'Intercambiar',
        }
    })
    .state('main.success-buy',{
        url: '/compra-exitosa',
        templateUrl: 'views/main.success-buy.html',
        data:{
            title: 'Compra exitosa',
        }
    })
    .state('main.userss',{
        url: '/users',
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl',
        resolve: {
            users: function(UserResource){
                return UserResource.query().$promise;
            }
        }
    })
    .state('main.userss.profile',{
        url: '/profile/:email',
        views: {
            profile: {
                templateUrl: 'views/userprofile.html',
                controller: 'Usersprofile2Ctrl',
                resolve: {
                    user: function(users, $stateParams){
                        return _.find(users, function(user){
                            return user.email == $stateParams.email
                        })
                    },
                    reportTypes: function(ReportsResource){
                        return ReportsResource.getReportTypes().$promise
                    }
                }
            }
        }
    });
}]);
