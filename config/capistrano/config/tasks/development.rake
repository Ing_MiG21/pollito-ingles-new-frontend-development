after :deploy ,'after_deploy' do
    on roles([:app]) do |host|
        deploy_path = deploy_to + '/current'
        application = 'pollito_frontend'
        execute("bash -l -c \"cd #{deploy_path} && npm install && bower install --allow-root \"")
        execute("bash -l -c \"cd #{deploy_path} && gulp build\"")
    end
end
