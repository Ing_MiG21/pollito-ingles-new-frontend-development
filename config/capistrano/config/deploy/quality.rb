role :app, %w{deploy@test.pollitoingles.geekies.co:5388}

set :stage, :deploy
server 'test.pollitoingles.geekies.co', user: 'deploy', roles: %w{app}, port: 5388

set :branch, 'quality'

set :deploy_to, '/home/deploy/pollito_frontend'
set :application, 'pollito_frontend'