role :app, %w{deploy@dev.pollitoingles.geekies.co:5388}

set :stage, :deploy
server 'dev.pollitoingles.geekies.co', user: 'deploy', roles: %w{app}, port: 5388

set :branch, 'development'

set :deploy_to, '/home/deploy/pollito_frontend'
set :application, 'pollito_frontend'