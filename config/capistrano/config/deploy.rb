set :application, 'pollito_frontend'
set :repo_url, 'git@gitlab.services4geeks.co:pollito-ingles/pollito-ingles-new-frontend.git'

set :linked_files, %w{ app/scripts/services/baseurl.js }
set :linked_dirs, %w{ bower_components node_modules }